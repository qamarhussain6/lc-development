namespace LeaseCrunch.Entitie
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VUsersRole
    {
        public Guid? Id { get; set; }

        [Key]
        [Column(Order = 0)]
        public string RoleId { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int RouteControlId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Controller { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(50)]
        public string Action { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool status { get; set; }

        [Key]
        [Column(Order = 5)]
        public string UserId { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(256)]
        public string UserName { get; set; }
    }
}
