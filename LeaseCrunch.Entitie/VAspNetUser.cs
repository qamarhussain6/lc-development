namespace LeaseCrunch.Entitie
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VAspNetUser
    {
        [Key]
        [Column(Order = 0)]
        public string Id { get; set; }

        public string Address { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string PostalCode { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        [Key]
        [Column(Order = 1)]
        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string PhoneNumber { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool PhoneNumberConfirmed { get; set; }

        [Key]
        [Column(Order = 3)]
        public bool TwoFactorEnabled { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        [Key]
        [Column(Order = 4)]
        public bool LockoutEnabled { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AccessFailedCount { get; set; }

        [Key]
        [Column(Order = 6)]
        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(128)]
        public string ParentId { get; set; }

        public int? UserLevel { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(256)]
        public string Role { get; set; }

        public int? ResellerId { get; set; }

        public int? Status { get; set; }
    }
}
