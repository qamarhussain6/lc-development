namespace LeaseCrunch.Entitie
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VCustomer
    {
        [Key]
        [Column(Order = 0)]
        public string Id { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Reseller { get; set; }

        [Key]
        [Column(Order = 2)]
        public bool EmailConfirmed { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Leases { get; set; }

        public int? UserLevel { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(256)]
        public string UserName { get; set; }

        [StringLength(256)]
        public string Email { get; set; }

        public int? ResellerId { get; set; }
    }
}
