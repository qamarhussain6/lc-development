namespace LeaseCrunch.Entitie
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("RolesRouteControl")]
    public partial class RolesRouteControl
    {
        public int Id { get; set; }

        public int RouteControlId { get; set; }

        [Required]
        [StringLength(128)]
        public string RoleId { get; set; }

        public virtual AspNetRole AspNetRole { get; set; }

        public virtual RouteControl RouteControl { get; set; }
    }
}
