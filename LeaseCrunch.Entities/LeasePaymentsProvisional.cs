namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeasePaymentsProvisional")]
    public partial class LeasePaymentsProvisional : Entity
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Amount { get; set; }

        public int Terms { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        public int PaymentFrequencyId { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }

        public virtual Lease Lease { get; set; }

        public virtual PaymentFrequency PaymentFrequency { get; set; }
    }
}
