namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("Lease")]
    public partial class Lease : Entity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Lease()
        {
            CustomizationCustomFieldsValues = new HashSet<CustomizationCustomFieldsValue>();
            LeaseDocuments = new HashSet<LeaseDocument>();
            LeasePayments = new HashSet<LeasePayment>();
            LeasePaymentsProvisionals = new HashSet<LeasePaymentsProvisional>();
            NoneLeasePayments = new HashSet<NoneLeasePayment>();
            NonLeasePaymentsProvisionals = new HashSet<NonLeasePaymentsProvisional>();
            LeaseGLAccounts = new HashSet<LeaseGLAccount>();
        }

        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal Id { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        [StringLength(250)]
        public string Manufacturer { get; set; }

        public int AssetTypeId { get; set; }

        public double? Size { get; set; }

        public int CompanyLocationId { get; set; }

        public int LesserId { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public int PaymentFrequencyId { get; set; }

        public int ROUAssetLife { get; set; }

        public int? Terms { get; set; }

        [Column(TypeName = "text")]
        public string Comments { get; set; }

        public int CostCenterId { get; set; }

        public int ClassificationId { get; set; }

        [Column(TypeName = "money")]
        public decimal DiscountRate { get; set; }

        [Column(TypeName = "money")]
        public decimal IncentiveReceived { get; set; }

        [Column(TypeName = "money")]
        public decimal InitialDirectCost { get; set; }

        public bool Status { get; set; }

        public int? CompanyAccountId { get; set; }

        public virtual Company Company { get; set; }

        public virtual CompanyAccount CompanyAccount { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationCustomFieldsValue> CustomizationCustomFieldsValues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeaseDocument> LeaseDocuments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeasePayment> LeasePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeasePaymentsProvisional> LeasePaymentsProvisionals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NoneLeasePayment> NoneLeasePayments { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NonLeasePaymentsProvisional> NonLeasePaymentsProvisionals { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeaseGLAccount> LeaseGLAccounts { get; set; }
    }
}
