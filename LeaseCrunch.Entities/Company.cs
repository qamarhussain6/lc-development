namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Company : Entity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Company()
        {
            Locations = new HashSet<Location>();
            Leases = new HashSet<Lease>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Location { get; set; }

        public int AccountingStandardId { get; set; }

        public int CountryId { get; set; }

        [StringLength(350)]
        public string Address1 { get; set; }

        [StringLength(350)]
        public string Address2 { get; set; }

        [StringLength(5)]
        public string Zip { get; set; }

        [Required]
        [StringLength(10)]
        public string City { get; set; }

        public int StateId { get; set; }

        public bool Status { get; set; }

        public int? CustomerId { get; set; }

        public virtual AccountingStandard AccountingStandard { get; set; }

        public virtual Country Country { get; set; }

        public virtual Customer Customer { get; set; }

        public virtual State State { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lease> Leases { get; set; }
    }
}
