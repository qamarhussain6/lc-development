namespace LeaseCrunch.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Repository.Pattern.Ef6;

    public partial class LeaseCrunchContext : DataContext
    {
        public LeaseCrunchContext()
            : base("name=LeaseCrunchContext")
        {
        }

        public virtual DbSet<AccountingStandard> AccountingStandards { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Classification> Classifications { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyAccount> CompanyAccounts { get; set; }
        public virtual DbSet<CompanyAccountsConfiguration> CompanyAccountsConfigurations { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomizationAssetType> CustomizationAssetTypes { get; set; }
        public virtual DbSet<CustomizationCostCenter> CustomizationCostCenters { get; set; }
        public virtual DbSet<CustomizationCustomField> CustomizationCustomFields { get; set; }
        public virtual DbSet<CustomizationCustomFieldsValue> CustomizationCustomFieldsValues { get; set; }
        public virtual DbSet<Lease> Leases { get; set; }
        public virtual DbSet<LeaseDocument> LeaseDocuments { get; set; }
        public virtual DbSet<LeaseGLAccount> LeaseGLAccounts { get; set; }
        public virtual DbSet<LeasePayment> LeasePayments { get; set; }
        public virtual DbSet<LeasePaymentsProvisional> LeasePaymentsProvisionals { get; set; }
        public virtual DbSet<Lesser> Lessers { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<NoneLeasePayment> NoneLeasePayments { get; set; }
        public virtual DbSet<NonLeasePaymentsProvisional> NonLeasePaymentsProvisionals { get; set; }
        public virtual DbSet<PaymentFrequency> PaymentFrequencies { get; set; }
        public virtual DbSet<Reseller> Resellers { get; set; }
        public virtual DbSet<RolesRouteControl> RolesRouteControls { get; set; }
        public virtual DbSet<RouteControl> RouteControls { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<VAspNetUser> VAspNetUsers { get; set; }
        public virtual DbSet<VCompanyAccount> VCompanyAccounts { get; set; }
        public virtual DbSet<VCustomerAccount> VCustomerAccounts { get; set; }
        public virtual DbSet<VCustomer> VCustomers { get; set; }
        public virtual DbSet<VResellerCustomer> VResellerCustomers { get; set; }
        public virtual DbSet<VReseller> VResellers { get; set; }
        public virtual DbSet<VUsersRole> VUsersRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountingStandard>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.AccountingStandard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts)
                .WithRequired(e => e.AccountType)
                .HasForeignKey(e => e.GLAccountType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts1)
                .WithRequired(e => e.AccountType1)
                .HasForeignKey(e => e.Classification)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts2)
                .WithRequired(e => e.AccountType2)
                .HasForeignKey(e => e.FinancialStatement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.RolesRouteControls)
                .WithRequired(e => e.AspNetRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<Classification>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Leases)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyAccount>()
                .Property(e => e.CustomerGLDescription)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyAccount>()
                .Property(e => e.GLNumber)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyAccount>()
                .HasMany(e => e.NonLeasePaymentsProvisionals)
                .WithRequired(e => e.CompanyAccount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyAccountsConfiguration>()
                .Property(e => e.CustomerGLDescription)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyAccountsConfiguration>()
                .Property(e => e.GLNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CompanyAccounts)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CustomizationCustomField>()
                .HasMany(e => e.CustomizationCustomFieldsValues)
                .WithRequired(e => e.CustomizationCustomField)
                .HasForeignKey(e => e.CustomizationCustomFieldsId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CustomizationCustomFieldsValue>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<CustomizationCustomFieldsValue>()
                .Property(e => e.value)
                .IsUnicode(false);

            modelBuilder.Entity<Lease>()
                .Property(e => e.Id)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Lease>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Lease>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Lease>()
                .Property(e => e.Manufacturer)
                .IsUnicode(false);

            modelBuilder.Entity<Lease>()
                .Property(e => e.Comments)
                .IsUnicode(false);

            modelBuilder.Entity<Lease>()
                .Property(e => e.DiscountRate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Lease>()
                .Property(e => e.IncentiveReceived)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Lease>()
                .Property(e => e.InitialDirectCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Lease>()
                .HasMany(e => e.CustomizationCustomFieldsValues)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lease>()
                .HasMany(e => e.LeaseDocuments)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lease>()
                .HasMany(e => e.LeasePayments)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lease>()
                .HasMany(e => e.LeasePaymentsProvisionals)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);


            modelBuilder.Entity<Lease>()
                .HasMany(e => e.NoneLeasePayments)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lease>()
                .HasMany(e => e.NonLeasePaymentsProvisionals)
                .WithRequired(e => e.Lease)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeaseDocument>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<LeaseDocument>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<LeaseDocument>()
                .Property(e => e.Size)
                .HasPrecision(18, 0);

            modelBuilder.Entity<LeaseDocument>()
                .Property(e => e.Path)
                .IsUnicode(false);

            modelBuilder.Entity<LeasePayment>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<LeasePaymentsProvisional>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Lesser>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .HasMany(e => e.RouteControls)
                .WithRequired(e => e.Module)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Lease>()
               .HasMany(e => e.LeaseGLAccounts)
               .WithRequired(e => e.Lease)
               .WillCascadeOnDelete(false);

            modelBuilder.Entity<LeaseGLAccount>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<NoneLeasePayment>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<NonLeasePaymentsProvisional>()
                .Property(e => e.Id)
                .HasPrecision(18, 0);

            modelBuilder.Entity<NonLeasePaymentsProvisional>()
                .Property(e => e.LeaseId)
                .HasPrecision(18, 0);

            modelBuilder.Entity<PaymentFrequency>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<PaymentFrequency>()
                .HasMany(e => e.LeasePaymentsProvisionals)
                .WithRequired(e => e.PaymentFrequency)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reseller>()
                .Property(e => e.City)
                .IsFixedLength();

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .HasMany(e => e.RolesRouteControls)
                .WithRequired(e => e.RouteControl)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VCompanyAccount>()
               .Property(e => e.CustomerGLDescription)
               .IsUnicode(false);

            modelBuilder.Entity<VCompanyAccount>()
                .Property(e => e.GLNumber)
                .IsUnicode(false);

            modelBuilder.Entity<VCompanyAccount>()
                .Property(e => e.btn)
                .IsUnicode(false);

            modelBuilder.Entity<VUsersRole>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<VUsersRole>()
                .Property(e => e.Action)
                .IsUnicode(false);
        }
    }
}
