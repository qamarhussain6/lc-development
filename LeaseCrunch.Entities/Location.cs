namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Location : Entity
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        [Required]
        [StringLength(250)]
        public string LocationName { get; set; }

        public int CountryId { get; set; }

        [StringLength(250)]
        public string Address1 { get; set; }

        [Required]
        [StringLength(250)]
        public string Address2 { get; set; }

        [StringLength(5)]
        public string Zip { get; set; }

        [StringLength(10)]
        public string City { get; set; }

        public int StateId { get; set; }

        public virtual Company Company { get; set; }

        public virtual Country Country { get; set; }

        public virtual State State { get; set; }
    }
}
