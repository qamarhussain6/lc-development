namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LeasePayment : Entity
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PaymentAmt { get; set; }

        public int PaymentsTerm { get; set; }

        [Column(TypeName = "date")]
        public DateTime PaymentStart { get; set; }

        [Column(TypeName = "date")]
        public DateTime PaymentEnd { get; set; }

        public virtual Lease Lease { get; set; }
    }
}
