namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CompanyAccountsConfiguration")]
    public partial class CompanyAccountsConfiguration : Entity
    {
        public int Id { get; set; }

        public int GLAccountType { get; set; }

        [Required]
        [StringLength(100)]
        public string CustomerGLDescription { get; set; }

        [Required]
        [StringLength(100)]
        public string GLNumber { get; set; }

        public int Classification { get; set; }

        public int FinancialStatement { get; set; }

        public bool NonLease { get; set; }

        public bool FinanceLeases { get; set; }
    }
}
