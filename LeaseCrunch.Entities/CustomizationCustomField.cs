namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomizationCustomField : Entity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CustomizationCustomField()
        {
            CustomizationCustomFieldsValues = new HashSet<CustomizationCustomFieldsValue>();
        }

        public int Id { get; set; }

        [StringLength(250)]
        public string Name { get; set; }

        public int? CustomerId { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationCustomFieldsValue> CustomizationCustomFieldsValues { get; set; }
    }
}
