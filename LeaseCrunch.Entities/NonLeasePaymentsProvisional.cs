namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("NonLeasePaymentsProvisional")]
    public partial class NonLeasePaymentsProvisional : Entity
    {
        [Column(TypeName = "numeric")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Amount { get; set; }

        public int Terms { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        public int CompanyAccountId { get; set; }

        public int PaymentFrequencyId { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }

        public virtual CompanyAccount CompanyAccount { get; set; }

        public virtual Lease Lease { get; set; }
    }
}
