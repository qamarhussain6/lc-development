namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LeaseGLAccount : Entity
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        public int CashAP { get; set; }

        public int ROUAsset { get; set; }

        public int LTLeaseLiability { get; set; }

        public int STLeaseLiability { get; set; }

        public int? RentExpense { get; set; }

        public int? VariableRentExpense { get; set; }

        public int? OtherPLAccount { get; set; }

        public int? OtherBalanceSheetAccount { get; set; }

        public int GLDescription { get; set; }

        public int PaymentFrequency { get; set; }

        public int? InterestExpense { get; set; }

        public int? AmortExpense { get; set; }

        public virtual Lease Lease { get; set; }
    }
}
