namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LeaseDocument")]
    public partial class LeaseDocument : Entity
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Size { get; set; }

        public DateTime UploadDate { get; set; }

        [Required]
        [StringLength(150)]
        public string Path { get; set; }

        public virtual Lease Lease { get; set; }
    }
}
