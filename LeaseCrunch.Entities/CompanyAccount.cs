namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompanyAccount : Entity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CompanyAccount()
        {
            Leases = new HashSet<Lease>();
            NonLeasePaymentsProvisionals = new HashSet<NonLeasePaymentsProvisional>();
        }

        public int Id { get; set; }

        public int CompanyId { get; set; }

        public int CustomerId { get; set; }

        public int GLAccountType { get; set; }

        [Required]
        [StringLength(100)]
        public string CustomerGLDescription { get; set; }

        [Required]
        [StringLength(100)]
        public string GLNumber { get; set; }

        public int Classification { get; set; }

        public int FinancialStatement { get; set; }

        public bool NonLease { get; set; }

        public bool FinanceLeases { get; set; }

        public virtual AccountType AccountType { get; set; }

        public virtual AccountType AccountType1 { get; set; }

        public virtual AccountType AccountType2 { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lease> Leases { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NonLeasePaymentsProvisional> NonLeasePaymentsProvisionals { get; set; }
    }
}
