namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class VCompanyAccount : Entity
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string Companies { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CustomerId { get; set; }

        [StringLength(250)]
        public string GLAccountType { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(100)]
        public string CustomerGLDescription { get; set; }

        [Key]
        [Column(Order = 4)]
        [StringLength(100)]
        public string GLNumber { get; set; }

        [StringLength(250)]
        public string FinancialStatement { get; set; }

        [StringLength(250)]
        public string Classification { get; set; }

        [Key]
        [Column(Order = 5)]
        public bool NonLease { get; set; }

        [Key]
        [Column(Order = 6)]
        public bool FinanceLeases { get; set; }

        [Key]
        [Column(Order = 7)]
        [StringLength(50)]
        public string Name { get; set; }

        [Key]
        [Column(Order = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CompanyId { get; set; }

        [StringLength(60)]
        public string btn { get; set; }
    }
}
