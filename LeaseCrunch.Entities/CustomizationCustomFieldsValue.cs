namespace LeaseCrunch.Entities
{
    using Repository.Pattern.Ef6;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CustomizationCustomFieldsValue : Entity
    {
        public int id { get; set; }

        public int CustomizationCustomFieldsId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [StringLength(500)]
        public string value { get; set; }

        public virtual CustomizationCustomField CustomizationCustomField { get; set; }

        public virtual Lease Lease { get; set; }
    }
}
