namespace LeaseCrunchContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CompanyAccount
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        public int CustomerId { get; set; }

        public int GLAccountType { get; set; }

        [Required]
        [StringLength(100)]
        public string CustomerGLDescription { get; set; }

        [Required]
        [StringLength(100)]
        public string GLNumber { get; set; }

        public int Classification { get; set; }

        public int FinancialStatement { get; set; }

        public bool NonLease { get; set; }

        public bool FinanceLeases { get; set; }

        public virtual AccountType AccountType { get; set; }

        public virtual AccountType AccountType1 { get; set; }

        public virtual AccountType AccountType2 { get; set; }

        public virtual Company Company { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
