namespace LeaseCrunchContext
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Customer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Customer()
        {
            AspNetUsers = new HashSet<AspNetUser>();
            CompanyAccounts = new HashSet<CompanyAccount>();
            CustomizationAssetTypes = new HashSet<CustomizationAssetType>();
            CustomizationCostCenters = new HashSet<CustomizationCostCenter>();
            CustomizationCustomFields = new HashSet<CustomizationCustomField>();
            CustomizationLessors = new HashSet<CustomizationLessor>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public bool Status { get; set; }

        public bool? EnableResellerAccesstomyAccount { get; set; }

        public bool? RequireLeaseTermGuidanceWizardforeveryLease { get; set; }

        public bool? RequireClassificationWizardforeveryLease { get; set; }

        public int? ResellerId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUser> AspNetUsers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyAccount> CompanyAccounts { get; set; }

        public virtual Reseller Reseller { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationAssetType> CustomizationAssetTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationCostCenter> CustomizationCostCenters { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationCustomField> CustomizationCustomFields { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomizationLessor> CustomizationLessors { get; set; }
    }
}
