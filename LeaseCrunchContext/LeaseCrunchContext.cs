namespace LeaseCrunchContext
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LeaseCrunchContext : DbContext
    {
        public LeaseCrunchContext()
            : base("name=LeaseCrunchContext")
        {
        }

        public virtual DbSet<AccountingStandard> AccountingStandards { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<CompanyAccount> CompanyAccounts { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomizationAssetType> CustomizationAssetTypes { get; set; }
        public virtual DbSet<CustomizationCostCenter> CustomizationCostCenters { get; set; }
        public virtual DbSet<CustomizationCustomField> CustomizationCustomFields { get; set; }
        public virtual DbSet<CustomizationLessor> CustomizationLessors { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Reseller> Resellers { get; set; }
        public virtual DbSet<RolesRouteControl> RolesRouteControls { get; set; }
        public virtual DbSet<RouteControl> RouteControls { get; set; }
        public virtual DbSet<State> States { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<VAspNetUser> VAspNetUsers { get; set; }
        public virtual DbSet<VCustomer> VCustomers { get; set; }
        public virtual DbSet<VResellerCustomer> VResellerCustomers { get; set; }
        public virtual DbSet<VReseller> VResellers { get; set; }
        public virtual DbSet<VUsersRole> VUsersRoles { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountingStandard>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.AccountingStandard)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts)
                .WithRequired(e => e.AccountType)
                .HasForeignKey(e => e.GLAccountType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts1)
                .WithRequired(e => e.AccountType1)
                .HasForeignKey(e => e.Classification)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountType>()
                .HasMany(e => e.CompanyAccounts2)
                .WithRequired(e => e.AccountType2)
                .HasForeignKey(e => e.FinancialStatement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.RolesRouteControls)
                .WithRequired(e => e.AspNetRole)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AspNetRole>()
                .HasMany(e => e.AspNetUsers)
                .WithMany(e => e.AspNetRoles)
                .Map(m => m.ToTable("AspNetUserRoles").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserClaims)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<AspNetUser>()
                .HasMany(e => e.AspNetUserLogins)
                .WithRequired(e => e.AspNetUser)
                .HasForeignKey(e => e.UserId);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Company>()
                .HasMany(e => e.CompanyAccounts)
                .WithRequired(e => e.Company)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CompanyAccount>()
                .Property(e => e.CustomerGLDescription)
                .IsUnicode(false);

            modelBuilder.Entity<CompanyAccount>()
                .Property(e => e.GLNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.Country)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Customer>()
                .HasMany(e => e.CompanyAccounts)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Module>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Module>()
                .HasMany(e => e.RouteControls)
                .WithRequired(e => e.Module)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Reseller>()
                .Property(e => e.City)
                .IsFixedLength();

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Action)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<RouteControl>()
                .HasMany(e => e.RolesRouteControls)
                .WithRequired(e => e.RouteControl)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Companies)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<State>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.State)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VUsersRole>()
                .Property(e => e.Controller)
                .IsUnicode(false);

            modelBuilder.Entity<VUsersRole>()
                .Property(e => e.Action)
                .IsUnicode(false);
        }
    }
}
