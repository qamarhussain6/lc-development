﻿$(document).ready(function () {
    //var baseUrl = '@Url.Action("List","Account")';
    var url = getURL(baseUrl);

    var table = $('#UserList').DataTable({
        bPaginate: false,
        serverSide: false,
        searching: false,
        destroy: true,
        processing: true,
        ajax:
            {
                "url": url
            },
        columns: [
    { "data": "name", "orderable": true },
    { "data": "email", "orderable": true },
    { "data": "role", "orderable": true }
        ],
        columnDefs: [{
            "targets": 3,
            "data": null,
            "defaultContent": '<i class="fa fa-pencil icon-edit"></i><i class="fa fa-ban icon-delete"></i>'
        }]
    });
    //Include disabled accounts
    $('body').on('click', '#showfilter', function () {
        status = $('#disabledaccount').is(':checked');
        filter = $('#filter').val();
        table.ajax.url(getURL(baseUrl)).load();

        if (filter.trim() == '') {
            message = 'No search criteria to perform';
        } else {
            message = 'Record has been filtered';
        }
        toastr.success(message, 'SUCCESS', { timeOut: 5000, closeButton: true });

    });

    //Include disabled accounts
    $('body').on('click', '#disabledaccount', function () {
        status = $('#disabledaccount').is(':checked');
        filter = $('#filter').val();
        table.ajax.url(getURL(baseUrl)).load();
        if (status == 'true') {
            message = 'Disabled users included in grid';
        } else {
            message = 'Disabled users excluded from grid';
        }
        toastr.success(message, 'SUCCESS', { timeOut: 5000, closeButton: true });
    });

    // table.ajax.reload();
    //invite new LC admin
    $('body').on('click', '#showinvite', function () {
        $("#modalheading").text("Invite LeaseCrunch Admin");
        //$('input[name="userid"]').attr('value', "-1");
        $("#userid").val('00000000-0000-0000-0000-000000000000');
        $("#firstname").val('');
        $("#lastname").val('');
        $("#email").val('');
        $("#UserModal").modal('show');
    });

    $('#UserList tbody').on('click', 'i', function () {

        var type = $(this).attr("class");
        var data = table.row($(this).parents('tr')).data();

        if (type == 'fa fa-pencil icon-edit') {
            $("#modalheading").text("Add / Edit User");
            $("#userid").val(data.id);
            $("#firstname").val(data.firstname);
            $("#lastname").val(data.lastname);
            $("#email").val(data.email);
            $("#roles option").each(function () { this.selected = $(this).text() == data.roles; });
            $("#UserModal").modal('show');
        }
        else if (type == 'fa fa-ban icon-delete') {
            $(this).confirmation({
                onConfirm: function () {
                    $.ajax({
                        url: deleteUrl,
                        type: 'POST',
                        data: {
                            id: data.id
                        },
                        success: function (response) {
                            table.ajax.url(getURL(baseUrl)).load();
                        },
                        error: function (msg) {
                            alert('Exception \n' + response.status);
                        }
                    });
                }
            });
        }
    });

    //$('#frm').validator().on('submit', function (e) {
    //    if (e.isDefaultPrevented()) {
    //    } else {
    //        $.ajax({
    //            url: '@Url.Action("Save", "Account")',
    //            type: 'POST',
    //            data: {
    //                id: $('#userid').val(),
    //                firstname: $('#firstname').val(),
    //                lastname: $('#lastname').val(),
    //                email: $('#email').val(),
    //                roles: $('#roles').text()
    //            },
    //            success: function () {
    //                status = $('#disabledaccount').is(':checked');
    //                filter = $('#filter').val();
    //                table.ajax.url(getURL(baseUrl)).load();


    //                alert($("#userid").val());
    //                if ($("#userid").val('00000000-0000-0000-0000-000000000000')){
    //                    message = 'Record has been inserted';
    //                }else{
    //                    message = 'Record has been updated';
    //                }
    //                toastr.success(message, 'SUCCESS', { timeOut: 5000, closeButton: true });

    //                $("#UserModal").modal('hide');


    //            },
    //            error: function (msg) {
    //                alert('Exception \n' + response.status);
    //            }


    //        });
    //    }
    //});
    
});

var checkTab = function () {
    var $tab = $('#myTab'), $active = $tab.find('.active'), text = $active.find('a').text();
    return text;
};

var deleteCompany = function (a) {
    alert(a);
}

var loadGrid = function () {

}

var getURL = function (baseUrl) {
    var url = baseUrl + "?disable=" + $('#disabledaccount').is(':checked') + "&filter=" + $("#filter").val();
    //alert(url);
    return url;
}
