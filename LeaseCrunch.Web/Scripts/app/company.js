﻿var feature = (function () {

    // Private variables and functions
    var privateThing = "secret";
    var publicThing = "not secret";

    var changePrivateThing = function () {
        privateThing = "super secret";
    };

    var sayPrivateThing = function () {
        console.log(privateThing);
        changePrivateThing();
    };

    // Public API
    return {
        publicThing: publicThing,
        sayPrivateThing: sayPrivateThing
    };
})();