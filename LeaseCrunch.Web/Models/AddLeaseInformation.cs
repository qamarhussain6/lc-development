﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using LeaseCrunch.Entities;

namespace LeaseCrunch.Web.Models
{

    public class LeaseViewModel
    {
        public LeaseViewModel()
        {
            leaseDocuments = new List<LeaseDocumentViewModel>();
        }
        public AddLeaseInformation AddLeaseInformation { get; set; }
        public List<LeaseDocumentViewModel> leaseDocuments { get; set; }
        public NonLeasePaymentsProvisionalViewModel nonLeasePayments { get; set; }
        public LeasePaymentsProvisionalViewModel leasePayments { get; set; }


    }
    public class AddLeaseInformation
    {
        //Id int
        //CompanyId   int

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Title")]
        public int? Id { get; set; }
        public string Title { get; set; }
        public int CompanyId { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public int AssetTypeId { get; set; }
        public double? Size { get; set; }
        public int CompanyLocationId { get; set; }
        public int LesserId { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
        public int PaymentFrequencyId { get; set; }
        public int ROUAssetLife { get; set; }
        public int? Terms { get; set; }
        public string Comments { get; set; }
        public int CostCenterId { get; set; }
        public int ClassificationId { get; set; }
        public decimal DiscountRate { get; set; }
        public decimal IncentiveReceived { get; set; }
        public decimal InitialDirectCost { get; set; }
        public bool Status { get; set; }
    }


}