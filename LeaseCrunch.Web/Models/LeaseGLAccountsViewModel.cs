﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class LeaseGLAccountsViewModel
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        public int CashAP { get; set; }

        public int ROUAsset { get; set; }

        public int LTLeaseLiability { get; set; }

        public int STLeaseLiability { get; set; }

        public int? RentExpense { get; set; }

        public int? VariableRentExpense { get; set; }

        public int? OtherPLAccount { get; set; }

        public int? OtherBalanceSheetAccount { get; set; }

        public int GLDescription { get; set; }

        public int PaymentFrequency { get; set; }

        public int? InterestExpense { get; set; }

        public int? AmortExpense { get; set; }
    }
}