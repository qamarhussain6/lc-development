﻿namespace LeaseCrunch.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    public partial class LeasePaymentClassifciationModels
    {
        public int Id { get; set; }

        public int LeaseId { get; set; }

        public int DiscountRate { get; set; }

        public double IncentivesReceived { get; set; }

        public double InitialDirectCosts { get; set; }

        public int PaymentFrequency { get; set; }

        public int Classification { get; set; }
        

    }
}
