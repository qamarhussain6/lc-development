﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class Message
    {
        public bool Status { get; set; }
        public string message { get; set; }
    }

    public class Profile
    {
        public string id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
    }
}