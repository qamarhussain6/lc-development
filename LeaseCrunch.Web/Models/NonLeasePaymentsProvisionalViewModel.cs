﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class NonLeasePaymentsProvisionalViewModel
    {
        public int Id { get; set; }

        [Column(TypeName = "numeric")]
        public decimal LeaseId { get; set; }

        [Column(TypeName = "numeric")]
        public decimal PaymentAmt { get; set; }

        public int PaymentsTerm { get; set; }

        [Column(TypeName = "date")]
        public DateTime PaymentStart { get; set; }

        [Column(TypeName = "date")]
        public DateTime PaymentEnd { get; set; }
        public int PaymentFrequencyNonLease { get; set; }
    }
}