﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class LeasePaymentViewModel
    {
        public int Id { get; set; }
        public int LeaseId { get; set; }
        public decimal PaymentAmt { get; set; }
        public int PaymentsTerm { get; set; }
        public DateTime PaymentStart { get; set; }
        public DateTime PaymentEnd { get; set; }
    }
}