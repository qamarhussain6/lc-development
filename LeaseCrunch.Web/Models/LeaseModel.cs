﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class LeaseSection
    {
        public LeaseSection()
        {
            LeaseTypes = new List<LeaseType>();
        }
        public bool IsLease { get; set; }
        public string Name { get; set; }
        public List<LeaseType> LeaseTypes { get; set; }
    }

    public class LeaseType
    {
        public LeaseType()
        {
            LeaseDetail = new List<LeaseDetail>();
        }
        public string Name { get; set; }
        public string btn { get; set; }
        public string BtnCaption { get; set; }
        public List<LeaseDetail> LeaseDetail { get; set; }
        public List<LeaseList> LeaseList { get; set; }
    }

    public class LeaseDetail
    {
        public int Id { get; set; }
        public string Companies { get; set; }
        public int? CustomerId { get; set; }
        public string GLAccountType { get; set; }
        public string CustomerGLDescription { get; set; }
        public string GLNumber { get; set; }
        public string FinancialStatement { get; set; }
        public string Classification { get; set; }
        public bool? NonLease { get; set; }
        
        public string Name { get; set; }
        public int CompanyId { get; set; }
        public bool FinanceLeases { get; internal set; }
    }

    public class LeaseTypeModel
    {
        public bool TypeId { get; set; }
        public string Name { get; set; }
        public string btn { get; set; }
        public string BtnCaption { get; set; }
        public List<LeaseList> LeaseList { get; set; }
    }

    public class LeaseList
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}