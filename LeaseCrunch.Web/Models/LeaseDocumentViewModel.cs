﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Models
{
    public class LeaseDocumentViewModel
    {
        public int Id { get; set; }
        public int LeaseId { get; set; }
        public string Name { get; set; }
        public decimal Size { get; set; }
        public DateTime UploadDate { get; set; }
        public string Path { get; set; }
    }
}