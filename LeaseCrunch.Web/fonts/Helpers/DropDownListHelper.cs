﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LeaseCrunch.Web.Helpers
{
    public class DropDownListHelper
    {
        public static string DropDownList(string target, string text)
        {
            return String.Format("<label for='{0}'>{1}</label>", target, text);
        }
    }
}