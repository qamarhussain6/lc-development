using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using LeaseCrunch.Service;
using Repository.Pattern.DataContext;
using Repository.Pattern.UnitOfWork;
using Repository.Pattern.Ef6;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Microsoft.AspNet.Identity;
using LeaseCrunch.Web.Models;
using LeaseCrunch.Web.Controllers;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LeaseCrunch.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here

            container
              .RegisterType<IDataContextAsync, LeaseCrunchContext>(new PerRequestLifetimeManager())
              .RegisterType<IUnitOfWorkAsync, UnitOfWork>(new PerRequestLifetimeManager())
              .RegisterType<IRepositoryAsync<CustomizationCostCenter>, Repository<CustomizationCostCenter>>()
              .RegisterType<ICustomizationCostCenterService, CustomizationCostCenterService>()
              .RegisterType<IRepositoryAsync<LeasePaymentsProvisional>, Repository<LeasePaymentsProvisional>>()
              .RegisterType<ILeasePaymentsProvisionalService, LeasePaymentsProvisionalService>()
              .RegisterType<IRepositoryAsync<NonLeasePaymentsProvisional>, Repository<NonLeasePaymentsProvisional>>()
              .RegisterType<INonLeasePaymentsProvisionalService, NonLeasePaymentsProvisionalService>()
              .RegisterType<IRepositoryAsync<LeaseDocument>, Repository<LeaseDocument>>()
              .RegisterType<ILeaseDocumentService,LeaseDocumentService>()
              .RegisterType<IRepositoryAsync<Lesser>, Repository<Lesser>>()
              .RegisterType<ILessersService, LessersService>()
              .RegisterType<IRepositoryAsync<Lease>, Repository<Lease>>()
              .RegisterType<ILeasesService, LeasesService>()
              .RegisterType<IRepositoryAsync<Classification>, Repository<Classification>>()
              .RegisterType<IClassificationService, ClassificationService>()
              .RegisterType<IRepositoryAsync<CompanyAccount>, Repository<CompanyAccount>>()
              .RegisterType<ICompanyAccountService, CompanyAccountService>()
              .RegisterType<IRepositoryAsync<CompanyAccountsConfiguration>, Repository<CompanyAccountsConfiguration>>()
              .RegisterType<ICompanyAccountsConfigurationService, CompanyAccountsConfigurationService>()
              .RegisterType<IRepositoryAsync<VCompanyAccount>, Repository<VCompanyAccount>>()
              .RegisterType<IVCompanyAccountService, VCompanyAccountService>()
              .RegisterType<IRepositoryAsync<VCustomerAccount>, Repository<VCustomerAccount>>()
              .RegisterType<IVCustomerAccountService, VCustomerAccountService>()
              .RegisterType<IRepositoryAsync<AccountType>, Repository<AccountType>>()
              .RegisterType<IAccountTypeService, AccountTypeService>()
              .RegisterType<IRepositoryAsync<CustomizationCustomField>, Repository<CustomizationCustomField>>()
              .RegisterType<ICustomizationCustomFieldService, CustomizationCustomFieldService>()
              .RegisterType<IRepositoryAsync<CustomizationCustomFieldsValue>, Repository<CustomizationCustomFieldsValue>>()
              .RegisterType<ICustomizationCustomFieldsValuesService, CustomizationCustomFieldsValuesService>()
              .RegisterType<IRepositoryAsync<Lesser>, Repository<Lesser>>()
              .RegisterType<ICustomizationLessorService, CustomizationLessorService>()
              .RegisterType<IRepositoryAsync<Module>, Repository<Module>>()
              .RegisterType<IModuleService, ModuleService>()
              .RegisterType<IRepositoryAsync<CustomizationAssetType>, Repository<CustomizationAssetType>>()
              .RegisterType<ICustomizationAssetTypeService, CustomizationAssetTypeService>()
              .RegisterType<IRepositoryAsync<VResellerCustomer>, Repository<VResellerCustomer>>()
              .RegisterType<IVResellerCustomerService, VResellerCustomerService>()
              .RegisterType<IRepositoryAsync<Country>, Repository<Country>>()
              .RegisterType<ICountryService, CountryService>()
              .RegisterType<IRepositoryAsync<Customer>, Repository<Customer>>()
              .RegisterType<ICustomerService, CustomerService>()
              .RegisterType<IRepositoryAsync<RolesRouteControl>, Repository<RolesRouteControl>>()
              .RegisterType<IRolesRouteControlService, RolesRouteControlService>()
              .RegisterType<IRepositoryAsync<RouteControl>, Repository<RouteControl>>()
              .RegisterType<IRouteControlService, RouteControlService>()
              .RegisterType<IRepositoryAsync<Company>, Repository<Company>>()
              .RegisterType<ICompanyService, CompanyService>()
              .RegisterType<IRepositoryAsync<Location>, Repository<Location>>()
              .RegisterType<ILocationService, LocationService>()
              .RegisterType<IRepositoryAsync<Reseller>, Repository<Reseller>>()
              .RegisterType<IResellerService, ResellerService>()
              .RegisterType<IRepositoryAsync<VReseller>, Repository<VReseller>>()
              .RegisterType<IVResellerService, VResellerService>()
              .RegisterType<IRepositoryAsync<AspNetUser>, Repository<AspNetUser>>()
              .RegisterType<IAspNetUserService, AspNetUserService>()
              .RegisterType<IRepositoryAsync<AspNetUserRole>, Repository<AspNetUserRole>>()
              .RegisterType<IAspNetUserRoleService, AspNetUserRoleService>()
              .RegisterType<IRepositoryAsync<AspNetRole>, Repository<AspNetRole>>()
              .RegisterType<IAspNetRoleService, AspNetRoleService>()
              .RegisterType<IRepositoryAsync<VCustomer>, Repository<VCustomer>>()
              .RegisterType<IVCustomerService, VCustomerService>()
              .RegisterType<IRepositoryAsync<VAspNetUser>, Repository<VAspNetUser>>()
              .RegisterType<IVAspNetUsersService, VAspNetUsersService>()
              .RegisterType<IRepositoryAsync<AccountingStandard>, Repository<AccountingStandard>>()
              .RegisterType<IAccountingStandardService, AccountingStandardService>()
              .RegisterType<IRepositoryAsync<State>, Repository<State>>()
              .RegisterType<IStateService, StateService>()
              .RegisterType<IUserStore<ApplicationUser>, UserStore<ApplicationUser>>()
              .RegisterType<UserManager<ApplicationUser>>()
              .RegisterType<ApplicationDbContext, ApplicationDbContext>()

                .RegisterType<IRepositoryAsync<LeaseDocument>, Repository<LeaseDocument>>()
              .RegisterType<ILeaseDocumentsService, LeaseDocumentsService>()

               .RegisterType<IRepositoryAsync<LeaseGLAccount>, Repository<LeaseGLAccount>>()
              .RegisterType<ILeaseGLService, LeaseGLService>()

              .RegisterType<ApplicationUserManager>()
              .RegisterType<AccountController>(new InjectionConstructor());
        }
    }
}
