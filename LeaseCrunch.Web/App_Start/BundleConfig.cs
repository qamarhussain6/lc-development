﻿using System.Web;
using System.Web.Optimization;

namespace LeaseCrunch.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/moment.js",                    //
                      "~/scripts/bootstrap-confirmation.js"));

            bundles.Add(new ScriptBundle("~/content/toastr").Include(
                      "~/Scripts/toastr.js",
                      "~/Scripts/app.js"));

            bundles.Add(new ScriptBundle("~/content/validator/js").Include(
                      "~/plugins/validator/js/validator.js",
                      "~/plugins/numeric/jquery.alphanumeric.js",
                      "~/plugins/numeric/jquery.numeric.js"
                      ));

            bundles.Add(new ScriptBundle("~/content/grid/js").Include(
                      "~/plugins/grid/grid.js"
                      ));

            bundles.Add(new StyleBundle("~/content/validator/css").Include(
                    "~/plugins/validator/css/docs.css",
                      "~/plugins/validator/css/pygments-manni.css"
                      ));

            bundles.Add(new ScriptBundle("~/content/datepicker/js").Include(
                        "~/plugins/datepicker/bootstrap-datepicker.js",
                        "~/Scripts/date.js",
                        "~/Scripts/moment.js"
                        ));
            bundles.Add(new ScriptBundle("~/content/steps/js").Include(
                        "~/Scripts/jquery.steps.js"
                        ));
            
            bundles.Add(new StyleBundle("~/content/css").Include(
                      "~/Content/css/forms.css",
                      "~/Content/css/colors.css",
                     "~/Content/css/variables.css",
                     "~/Content/css/helper.css",
                     "~/Content/css/buttons.css",
                     "~/Content/css/toastr-override.css",
                     "~/Content/css/style.css"
                      ));

            //Plugin for datatable
            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
          "~/plugins/datatables/jquery.dataTables.js",
          "~/Scripts/dataTables.responsive.min.js",             
          "~/Scripts/jquery.filtertable.js",                    
          "~/Scripts/jquery.steps.js",                          
          "~/Scripts/bootstrap-datetimepicker.js",              
          "~/Scripts/jquery.mCustomScrollbar.concat.min.js")      
          );

            bundles.Add(new StyleBundle("~/Content/css/Datatables").Include("~/Content/css/plugins/jquery.dataTables.css"));
        }
    }
}