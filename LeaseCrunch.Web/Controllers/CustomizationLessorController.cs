﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using Repository.Pattern.Infrastructure;
using LeaseCrunch.Web.Models;
using System.Data.Entity.Validation;

namespace LeaseCrunch.Web.Controllers
{
    public class CustomizationLessorController : Controller
    {
        private readonly ICustomizationLessorService _customizationLessorService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public CustomizationLessorController(IUnitOfWorkAsync unitOfWorkAsync, ICustomizationLessorService customizationLessorService)
        {
            _customizationLessorService = customizationLessorService;
            _unitOfWorkAsync = unitOfWorkAsync;
        }


        // GET: Companies/Create
        [HttpPost]
        public async Task<ActionResult> Save()
        {

            Message message = new Message
            {
                Status = true
            };
            Lesser lesser = new Lesser();
            string a = Request.Form["lessorid"];
            int id = Convert.ToInt32(Request.Form["lessorid"]);
            if (id > 0) { lesser = _customizationLessorService.Find(id); }

            lesser.Name = Request.Form["name"];
            lesser.CustomerId = Convert.ToInt16(Request.Form["customerid"]);
            if (id == -1)
            {
                _customizationLessorService.Insert(lesser);
            }
            else {
                lesser.ObjectState = ObjectState.Modified;
                _customizationLessorService.Update(lesser);
            }
            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                // throw;
                message.message = error;
                message.Status = false;

            }
            catch (Exception ex)
            {
            }
            return Json(message);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> Delete()
        {
            Message message = new Message
            {
                Status = true
            };
            try
            {
                int id = Convert.ToInt16(Request.Form["id"]);
                _customizationLessorService.Delete(id);
                _unitOfWorkAsync.SaveChanges();
            }
            catch (Exception ex)
            {
            }

            return Json(message);

        }


        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ActionResult List()
        {
            int CustomerId = 0;
            if (Session["customerid"] != null) {
                CustomerId = Convert.ToInt32(Session["customerid"].ToString());
            }
            var query = _customizationLessorService.Queryable().Where(x => x.CustomerId == CustomerId);
            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.Name
            });

            return Json(collection, JsonRequestBehavior.AllowGet);
        }
    }
}