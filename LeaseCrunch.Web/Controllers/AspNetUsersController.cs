﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.Web;
using System.Net.Mail;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using Repository.Pattern.Infrastructure;
using Microsoft.AspNet.Identity;
using LeaseCrunch.Web.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Reflection;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LeaseCrunch.Web.Controllers
{
    public class AspNetUsersController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;
        //public async Task<ActionResult> List()
        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.ToList();
            ViewBag.roles = new SelectList(roles, "Id", "Name");
            return View();
        }

        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ActionResult List(bool disable, string filter)
        {
            LeaseCrunchContext data = new LeaseCrunchContext();

            //var query = from user in db.Users select user;
            var query = from vw_AspNetUsers in data.VAspNetUsers select vw_AspNetUsers;

            List<VAspNetUser> users = new List<VAspNetUser>();
            if (disable == false)
                query = query.Where(x => x.EmailConfirmed != disable);

            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.UserName.Contains(filter) || x.Email.Contains(filter) || x.FirstName.Contains(filter) || x.LastName.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.FirstName + " " + x.LastName,
                firstname = x.FirstName,
                lastname = x.LastName,
                email = x.Email,
                role = x.Role
            });

            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

        }


        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var applicationUser = db.Users.Find(id);
            db.Users.Remove(applicationUser);
            Message message = new Message
            {
                Status = true
            };
            db.SaveChanges();
            return Json(message);

        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Save")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[ValidateAntiForgeryToken]
        //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
        public async Task<ActionResult> Save(Guid id)
        {

            Message message = new Message
            {
                Status = true
            };
            string e_mail = Request.Form["email"];
            var user = await UserManager.FindByNameAsync(e_mail);
            if (user == null)
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.UserName = applicationUser.Email = Request.Form["email"];
                applicationUser.EmailConfirmed = true;
                applicationUser.UserLevel = 0;
                applicationUser.ParentId = null;

                var result = await UserManager.CreateAsync(applicationUser, "P@ssw0rd");
                if (result.Succeeded)
                {
                    user = await UserManager.FindByNameAsync(e_mail);
                    string role = Request.Form["roles"].Trim().Replace('\n', ' ');
                    var roleresult = UserManager.AddToRole(user.Id, role);
                    var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("Register", "Account", new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);

                    StringBuilder email = new StringBuilder();
                    email.AppendLine("Hi " + Request.Form["firstname"] + " " + Request.Form["lastname"] + ",");
                    email.AppendLine("<br><br>");
                    email.AppendLine("You are invited to create a Admin Account on LeaseCrunch&#46;com.");
                    email.AppendLine("<br><br>");
                    email.AppendLine("Click on the link below to get started.");
                    email.AppendLine("<br><br>");
                    email.AppendLine("Create your account by clicking here:<a href=\"" + callbackUrl + "\">link</a> ");
                    email.AppendLine("<br><br>");
                    email.AppendLine("Thanks,");
                    email.AppendLine("<br><br>");
                    email.AppendLine("LeaseCrunch Support");

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.To.Add(new MailAddress(Request.Form["email"]));  // replace with valid value 
                    mailMessage.From = new MailAddress("support@lcsupport.com");  // replace with valid value
                    mailMessage.Subject = "LC Crunch Support";
                    mailMessage.Body = email.ToString();
                    mailMessage.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        await smtp.SendMailAsync(mailMessage);
                    }
                }
            }
            else
            {
                message.Status = false;
            }
            if (id == Guid.Empty)
            {

            }
            else
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser = db.Users.Find(id.ToString());
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.Email = Request.Form["email"];
                db.Entry(applicationUser).State = EntityState.Modified;
                await db.SaveChangesAsync();
                db.SaveChanges();
            }
            return Json(message);

        }




        //private readonly IAspNetUsersService _aspNetUsersService;
        //private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        //public AspNetUsersController(IUnitOfWorkAsync unitOfWorkAsync,
        //    IAspNetUsersService aspNetUsersService)
        //{
        //    _unitOfWorkAsync = unitOfWorkAsync;
        //    _aspNetUsersService = aspNetUsersService;
        //}      // GET: AspNetUsers

        //public async Task<ActionResult> Index()
        //{
        //    //return View(await db.AspNetUsers.ToListAsync());
        //    return View();
        //}

        //// GET: AspNetUsers/Details/5
        //public async Task<ActionResult> Details(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
        //    if (aspNetUsers == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(aspNetUsers);
        //}

        //// GET: AspNetUsers/Create
        //public ActionResult Create()
        //{
        //    return View();
        //}

        //// POST: AspNetUsers/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Create([Bind(Include = "Id,Address,City,State,PostalCode,Email,string1,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,ParentId,UserLevel,FirstName,LastName,Status")] AspNetUsers aspNetUsers)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.AspNetUsers.Add(aspNetUsers);
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }

        //    return View(aspNetUsers);
        //}

        //// GET: AspNetUsers/Edit/5
        //public async Task<ActionResult> Edit(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
        //    if (aspNetUsers == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(aspNetUsers);
        //}

        //// POST: AspNetUsers/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> Edit([Bind(Include = "Id,Address,City,State,PostalCode,Email,string1,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,ParentId,UserLevel,FirstName,LastName,Status")] AspNetUsers aspNetUsers)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(aspNetUsers).State = EntityState.Modified;
        //        await db.SaveChangesAsync();
        //        return RedirectToAction("Index");
        //    }
        //    return View(aspNetUsers);
        //}

        //// GET: AspNetUsers/Delete/5
        //public async Task<ActionResult> Delete(string id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
        //    if (aspNetUsers == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(aspNetUsers);
        //}

        //// POST: AspNetUsers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DeleteConfirmed(string id)
        //{
        //    AspNetUsers aspNetUsers = await db.AspNetUsers.FindAsync(id);
        //    db.AspNetUsers.Remove(aspNetUsers);
        //    await db.SaveChangesAsync();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}
