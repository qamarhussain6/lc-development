﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Repository.Pattern.UnitOfWork;
using LeaseCrunch.Web.Models;


namespace LeaseCrunch.Web.Controllers
{
    public class AccountTypeController : Controller
    {
        private readonly IAccountTypeService _leasesService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public AccountTypeController(IUnitOfWorkAsync unitOfWorkAsync, IAccountTypeService leasesService)
        {
            _leasesService = leasesService;
            _unitOfWorkAsync = unitOfWorkAsync;
        }
    }   
}