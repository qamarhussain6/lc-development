﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using LeaseCrunch.Common;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using Repository.Pattern.Infrastructure;
using LeaseCrunch.Web.Models;
using System.Data.Entity.Validation;
using System.Net.Mail;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Reflection;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System.Linq.Expressions;
using System.Collections;

namespace LeaseCrunch.Web.Controllers
{
    //[Authorize]
    public class CompaniesController : Controller
    {
        LeaseCrunchContext db = new LeaseCrunchContext();
        // ApplicationDbContext adb = new ApplicationDbContext();

        private readonly IAccountTypeService _accountTypeService;
        private readonly ICompanyAccountsConfigurationService _companyAccountsConfigurationService;
        private readonly ICompanyAccountService _companyAccountService;
        private readonly IVCompanyAccountService _vcompanyAccountService;
        private readonly ICustomizationAssetTypeService _customizationAssetTypeService;
        private readonly ICustomizationCustomFieldService _customizationCustomFieldService;
        private readonly ICustomizationCostCenterService _customizationCostCenterService;
        private readonly ICustomerService _customerService;
        private readonly ICompanyService _companyService;
        private readonly ILocationService _locationService;
        private readonly ICountryService _countryService;
        private readonly IAccountingStandardService _accountingStandardService;
        private readonly IStateService _stateService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public CompaniesController(IUnitOfWorkAsync unitOfWorkAsync,
            ICompanyService companyService, ICountryService countryService, IAccountingStandardService accountingStandardService, IStateService stateService, ILocationService locationService, ICustomerService customerService, ICustomizationAssetTypeService customizationAssetTypeService, ICustomizationCustomFieldService customizationCustomFieldService, ICustomizationCostCenterService customizationCostCenterService, IAccountTypeService accountTypeService, IVCompanyAccountService vcompanyAccountService, ICompanyAccountsConfigurationService companyAccountsConfigurationService, ICompanyAccountService companyAccountService)
        {
            _accountTypeService = accountTypeService;
            _customizationAssetTypeService = customizationAssetTypeService;
            _customizationCustomFieldService = customizationCustomFieldService;
            _customizationCostCenterService = customizationCostCenterService;
            _unitOfWorkAsync = unitOfWorkAsync;
            _companyService = companyService;
            _countryService = countryService;
            _accountingStandardService = accountingStandardService;
            _stateService = stateService;
            _locationService = locationService;
            _customerService = customerService;
            _vcompanyAccountService = vcompanyAccountService;
            _companyAccountsConfigurationService = companyAccountsConfigurationService;
            _companyAccountService = companyAccountService;
        }

        // GET: Companies
        public async Task<ActionResult> Index(int id = 0)
        {
            ApplicationUser user = new ApplicationUser();
            UserManager<ApplicationUser> UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            user = UserManager.FindById(User.Identity.GetUserId());
            ViewBag.UserLevel = user.UserLevel;

            if (id > 0)
            {
                Session["customerid"] = id;
            }
            else if (id == 0 && Session["customerid"] != null)
            {
                id = Convert.ToInt32(Session["customerid"].ToString());
            }
            else if (id == 0 && Session["customerid"] == null)
            {
                if (user != null)
                {
                    //id = user.CustomerId.Value;// (user.CustomerId == null) ? user.CustomerId.Value : 0;
                    Session["customerid"] = id = user.CustomerId.Value;
                }
            }
            
            var customer = _customerService.Queryable().Where(x => x.Id == id).FirstOrDefault();
            if (customer != null) Session["customer"] = customer.Name;

            var comp = _vcompanyAccountService.Queryable().Where(x => x.CustomerId == id).FirstOrDefault();
            if (comp != null)
            {
                Session["company"] = comp.Companies;
            }


            ApplicationDbContext adb = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(adb);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            string[] rolesid = { "2e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b", "3e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b", "5e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b" };
            var roles = roleMngr.Roles.Where(r => rolesid.Contains(r.Id)).ToList();
            ViewBag.roles = new SelectList(roles, "Id", "Name");

            ViewBag.rolesJson = JsonConvert.SerializeObject(roles);
            ViewBag.accountingStandardServiceJson = JsonConvert.SerializeObject(_accountingStandardService.Queryable().ToList());

            ViewBag.customizationAssetTypeService = _customizationAssetTypeService.Queryable().Where(x => x.CustomerId == id);
            ViewBag.customizationCustomFields = _customizationCustomFieldService.Queryable().Where(x => x.CustomerId == id);
            ViewBag.CustomizationCostCenters = _customizationCostCenterService.Queryable().Where(x => x.CustomerId == id); ;

            ViewBag.countryidl = new SelectList(_countryService.Queryable().ToList(), "Id", "Name");
            ViewBag.countryidc = new SelectList(_countryService.Queryable().ToList(), "Id", "Name");
            ViewBag.stateidl = new SelectList(_stateService.Queryable().ToList(), "Id", "Name", "id");
            ViewBag.stateidc = new SelectList(_stateService.Queryable().ToList(), "Id", "Name", "id");
            ViewBag.AccountingStandardId = new SelectList(_accountingStandardService.Queryable().ToList(), "Id", "Name");

            //Populate GL Account
            ViewBag.Leases_Operating_Leases = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Leases_Operating_Leases).ToList(), "Id", "Name", "--Select Operating Leases--");
            ViewBag.Leases_Finance_Leases = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Leases_Finance_Leases).ToList(), "Id", "Name");
            ViewBag.Non_Leases_Operating_Leases = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Non_Leases_Operating_Leases).ToList(), "Id", "Name");
            ViewBag.Non_Leases_Finance_Leases = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Non_Leases_Finance_Leases).ToList(), "Id", "Name");
            ViewBag.Classification = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Classification).ToList(), "Id", "Name");
            ViewBag.FinancialStatement = new SelectList(_accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.FinancialStatement).ToList(), "Id", "Name");

            PopulateCompany();

            return View();
        }

        [HttpGet]
        public JsonResult PopulateCompany()
        {
            int customerid = Convert.ToInt32(Session["customerid"]);
            var company = _companyService.Queryable().Where(c => c.CustomerId == customerid).ToList();
            var collection = company.Select(x => new
            {
                id = x.Id,
                name = x.Name

            }).ToList();

            ViewBag.company = new SelectList(collection, "id", "name", "--Select Company--");
            return Json(collection, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisplayAccounts(int id = 0)
        {
            List<bool> sections = new List<bool>();
            //var accounts = CompanyAccountService.Where(c => c.CompanyId == 1032).ToList();
            var accounts = _vcompanyAccountService.Queryable().Where(c => c.CompanyId == id).OrderBy(o => o.GLAccountType).ToList();

            sections.Add(true);
            sections.Add(false);

            var leaseSections = new List<LeaseSection>();

            foreach (var isLease in sections)
            {
                var leaseSection = new LeaseSection();
                leaseSection.IsLease = isLease;
                if (isLease == true)
                    leaseSection.Name = String.Format("GL Accounts for Lease Components for {0}", accounts.FirstOrDefault().Companies);
                else
                {
                    leaseSection.Name = "GL Ledger Accounts for Variable Expenses & Non-Lease Payments";
                }
                leaseSection.LeaseTypes = getTypes(accounts, isLease);
                leaseSections.Add(leaseSection);
            }

            //return JsonConvert.SerializeObject(leaseSections);
            return Json(leaseSections, JsonRequestBehavior.AllowGet);
        }

        private List<LeaseType> getTypes(List<VCompanyAccount> accounts, bool isLease)
        {
            var leaseTypes = new List<LeaseType>();
            foreach (var type in GetLeaseTypes())
            {
                var leaseType = new LeaseType();
                leaseType.Name = type.Name;
                leaseType.btn = type.btn;
                leaseType.BtnCaption = type.BtnCaption;
                leaseType.LeaseList = getLeaseList(Convert.ToBoolean(Convert.ToInt16(leaseType.btn)), isLease);
                var selectedLeases = accounts.Where(a => a.NonLease == isLease && a.FinanceLeases == type.TypeId).ToList();
                leaseType.LeaseDetail = getDetails(selectedLeases);
                leaseTypes.Add(leaseType);
            }
            return leaseTypes;
        }

        private List<LeaseList> getLeaseList(bool isFinance, bool isLease)
        {
            List<LeaseList> leaseList = new List<LeaseList>();
            if (isLease && !isFinance)
            {
                var objs = _accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Leases_Operating_Leases).ToList();
                foreach (var obj in objs)
                {
                    var lease = new LeaseList()
                    {
                        Value = obj.Id,
                        Text = obj.Name
                    };
                    leaseList.Add(lease);
                }
            }
            else if (isLease && isFinance)
            {
                var objs = _accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Leases_Finance_Leases).ToList();
                foreach (var obj in objs)
                {
                    var lease = new LeaseList()
                    {
                        Value = obj.Id,
                        Text = obj.Name
                    };
                    leaseList.Add(lease);
                }
            }
            else if (!isLease && !isFinance)
            {
                var objs = _accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Non_Leases_Operating_Leases).ToList();
                foreach (var obj in objs)
                {
                    var lease = new LeaseList()
                    {
                        Value = obj.Id,
                        Text = obj.Name
                    };
                    leaseList.Add(lease);
                }
            }
            else if (!isLease && isFinance)
            {
                var objs = _accountTypeService.Queryable().Where(x => x.ParentId == (int)Common.Leases.Non_Leases_Finance_Leases).ToList();
                foreach (var obj in objs)
                {
                    var lease = new LeaseList()
                    {
                        Value = obj.Id,
                        Text = obj.Name
                    };
                    leaseList.Add(lease);
                }
            }
            return leaseList;
        }

        private List<LeaseDetail> getDetails(IList<VCompanyAccount> accounts)
        {
            List<LeaseDetail> details = new List<LeaseDetail>();
            foreach (var account in accounts)
            {
                var leaseDetail = new LeaseDetail()
                {
                    Id = account.Id,
                    CompanyId = account.CompanyId,
                    GLAccountType = account.GLAccountType,
                    CustomerGLDescription = account.CustomerGLDescription,
                    GLNumber = account.GLNumber,
                    Classification = account.Classification,
                    FinancialStatement = account.FinancialStatement,
                    FinanceLeases = account.FinanceLeases
                };
                details.Add(leaseDetail);
            }

            return details;
        }

        private List<LeaseTypeModel> GetLeaseTypes()
        {
            var types = new List<LeaseTypeModel>();
            types.Add(new LeaseTypeModel()
            {
                TypeId = true,
                Name = "Operating Leases",
                btn = "0",
                BtnCaption = "Add Operating"
            });

            types.Add(new LeaseTypeModel()
            {
                TypeId = false,
                Name = "Finance Leases",
                btn = "1",
                BtnCaption = "Add Finance"
            });

            return types;

        }

        #region Companies
        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult ListCompany(string filter = "")
        {

            ViewBag.countryidc = new SelectList(_countryService.Queryable().ToList(), "Id", "Name");
            ViewBag.AccountingStandardId = new SelectList(_accountingStandardService.Queryable().ToList(), "Id", "Name");
            ViewBag.stateidc = new SelectList(_stateService.Queryable().ToList(), "Id", "Name");

            int customerid = Convert.ToInt32(Session["customerid"]);
            var company = _companyService.Queryable().Where(c => c.CustomerId == customerid).Include(c => c.AccountingStandard).Include(c => c.Country).Include(c => c.State).ToList();

            var collection = company.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                location = x.Location,
                accountingstandardid = x.AccountingStandard.Name,
                countryidc = x.Country.Name,
                address1 = x.Address1,
                address2 = x.Address2,
                zip = x.Zip,
                city = x.City,
                stateidc = x.State.Name,
                Status = x.Status
            }).ToList();

            //return Json(new { draw = 1, recordsTotal = collection.Count, recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);
            return Json( collection , JsonRequestBehavior.AllowGet);
        }

        // GET: Companies/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            return View(company);
        }

        // GET: Companies/Create
        [HttpPost]
        public async Task<ActionResult> SaveCompany()
        // public async Task<ActionResult> Save()
        {
            Company company = new Company();
            int companyid = 0;

            if (!string.IsNullOrEmpty(Request.Form["id"]))
                companyid = Convert.ToInt32(Request.Form["id"]);

            if (companyid > 0)
                company = _companyService.Find(companyid);

            company.Name = Request.Form["name"];
            company.Location = Request.Form["location"];
            company.AccountingStandardId = Convert.ToInt16(Request.Form["accountingstandardid"]);
            company.CountryId = Convert.ToInt16(Request.Form["countryidc"]);
            company.Address1 = Request.Form["address1"];
            company.Address2 = Request.Form["address2"];
            company.Zip = Request.Form["zip"];
            company.City = Request.Form["city"];
            company.StateId = Convert.ToInt16(Request.Form["stateidc"]);
            company.CustomerId = Convert.ToInt32(Session["customerid"]);
            company.Status = true;
            if (companyid > 0)
            {
                company.ObjectState = ObjectState.Modified;
                _companyService.Update(company);
            }
            else
            {
                _companyService.Insert(company);

                var companyAccountsConfigurations = _companyAccountsConfigurationService.Queryable().ToList();
                foreach (var companyAccountsConfiguration in companyAccountsConfigurations)
                {
                    CompanyAccount companyAccount = new CompanyAccount();
                    companyAccount.GLAccountType = companyAccountsConfiguration.GLAccountType;
                    companyAccount.CustomerId = Convert.ToInt32(Session["customerid"]);
                    companyAccount.CustomerGLDescription = companyAccountsConfiguration.CustomerGLDescription;
                    companyAccount.GLNumber = companyAccountsConfiguration.GLNumber;
                    companyAccount.Classification = companyAccountsConfiguration.Classification;
                    companyAccount.FinancialStatement = companyAccountsConfiguration.FinancialStatement;
                    companyAccount.NonLease = companyAccountsConfiguration.NonLease;
                    companyAccount.FinanceLeases = companyAccountsConfiguration.FinanceLeases;
                    _companyAccountService.Insert(companyAccount);
                }
            }

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                //throw;
            }
            catch (Exception ex)
            {


            }

            Message message = new Message
            {
                message = "Saved Sucessfully",
                Status = true
            };
            return Json(message);
        }


        // GET: Companies/Create
        [HttpPost]
        public async Task<ActionResult> UpdatePolicy()
        // public async Task<ActionResult> Save()
        {
            Message message = new Message()
            {
                Status = true,
                message = "Policy updated successfully"
            };
            Customer customer = new Customer();
            if (Session["customerid"] != null)
            {
                int customerid = Convert.ToInt16(Session["customerid"].ToString());
                try
                {
                    string enablereselleraccesstomyaccount = Request.Form["enablereselleraccesstomyaccount"];
                    customer = _customerService.Find(customerid);
                    customer.EnableResellerAccesstomyAccount = Convert.ToBoolean(Request.Form["enablereselleraccesstomyaccount"]);
                    customer.RequireLeaseTermGuidanceWizardforeveryLease = Convert.ToBoolean(Request.Form["requireleasetermguidancewizardforeverylease"]);
                    customer.RequireClassificationWizardforeveryLease = Convert.ToBoolean(Request.Form["requireclassificationwizardforeverylease"]);
                    //customer.ResellerId = Convert.ToInt32(Request.Form["resellerid"]);
                    customer.ObjectState = ObjectState.Modified;
                    _customerService.Update(customer);
                    int x = await _unitOfWorkAsync.SaveChangesAsync();
                }
                catch (DbEntityValidationException e)
                {
                    string error = string.Empty;
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                catch (Exception ex)
                {


                }

            }
            return Json(message);
        }

        public ActionResult PopulatePolicy()
        {
            int customerid = 0;
            if (Session["customerid"] != null)
            {
                customerid = Convert.ToInt16(Session["customerid"].ToString());
            }
            var customer = _customerService.Find(customerid);
            return Json(customer);
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Location,AccountingStandardId,CountryId,Address1,Address2,Zip,City,StateId,Status")] Company company)
        {
            if (ModelState.IsValid)
            {
                _companyService.Insert(company);
                await _unitOfWorkAsync.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.AccountingStandardId = new SelectList(db.AccountingStandards, "Id", "Name", company.AccountingStandardId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", company.CountryId);
            ViewBag.StateId = new SelectList(db.States, "Id", "Name", company.StateId);
            return View(company);
        }


        // GET: Companies/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Company company = await db.Companies.FindAsync(id);
            if (company == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountingStandardId = new SelectList(db.AccountingStandards, "Id", "Name", company.AccountingStandardId);
            ViewBag.CountryId = new SelectList(db.Countries, "Id", "Name", company.CountryId);
            ViewBag.StateId = new SelectList(db.States, "Id", "Name", company.StateId);
            return View(company);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Location,AccountingStandardId,CountryId,Address1,Address2,Zip,City,StateId,Status")] Company company)
        {
            if (ModelState.IsValid)
            {
                db.Entry(company).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.accountingstandardid = new SelectList(db.AccountingStandards, "Id", "Name", company.AccountingStandardId);
            ViewBag.countryId = new SelectList(db.Countries, "Id", "Name", company.CountryId);
            ViewBag.stateid = new SelectList(db.States, "Id", "Name", company.StateId);
            return View(company);
        }


        // POST: Companies/Delete/5
        [HttpPost, ActionName("DeleteCompany")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            var company = _companyService.Find(id);
            company.ObjectState = ObjectState.Deleted;

            _companyService.Delete(company);
            await _unitOfWorkAsync.SaveChangesAsync();

            Message message = new Message
            {
                Status = true
            };
            return Json(message);

        }
        #endregion

        #region Location

        [HttpPost]
        public async Task<ActionResult> SaveLocation()
        // public async Task<ActionResult> Save()
        {
            Location Location = new Location();
            int Locationid = 0;

            if (!string.IsNullOrEmpty(Request.Form["locationid"]))
                Locationid = Convert.ToInt32(Request.Form["locationid"]);

            if (Locationid > 0)
                Location = _locationService.Find(Locationid);

            Location.CompanyId = Convert.ToInt16(Request.Form["companyid"]);
            Location.LocationName = Request.Form["location"];
            Location.CountryId = Convert.ToInt16(Request.Form["countryidl"]);
            Location.Address1 = Request.Form["address1location"];
            Location.Address2 = Request.Form["address2location"];
            Location.Zip = Request.Form["ziplocation"];
            Location.City = Request.Form["citylocation"];
            Location.StateId = Convert.ToInt16(Request.Form["stateidl"]);
            if (Locationid > 0)
            {
                Location.ObjectState = ObjectState.Modified;
                _locationService.Update(Location);
            }
            else
            {
                _locationService.Insert(Location);
            }

            try
            {
                int x = await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string s = ex.ToString();

            }

            Message message = new Message
            {
                Status = true
            };
            return Json(message);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("DeleteLocation")]
        public async Task<ActionResult> DeleteLocation(int id)
        {
            var location = _locationService.Find(id);
            location.ObjectState = ObjectState.Deleted;

            _locationService.Delete(location);
            await _unitOfWorkAsync.SaveChangesAsync();

            Message message = new Message
            {
                Status = true
            };
            return Json(message);

        }



        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult ListLocation(int companyid = 0)
        {
            var location = _locationService.Queryable().Include(c => c.Country).Include(s => s.State).Where(l => l.CompanyId == companyid);

            var collection = location.Select(x => new
            {
                id = x.Id,
                locationname = x.LocationName,
                countryidl = x.Country.Name,
                address1 = x.Address1,
                address2 = x.Address2,
                zip = x.Zip,
                city = x.City,
                stateidl = x.State.Name
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
