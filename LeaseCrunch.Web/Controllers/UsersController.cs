﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using LeaseCrunch.Common;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Validation;
using LeaseCrunch.Web.Models;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Net.Mail;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using LeaseCrunch.Web.NavigationAttribute;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Repository.Pattern.Infrastructure;

namespace LeaseCrunch.Web.Controllers
{
    //  [AllowNavigationAttribute]
    [Authorize]
    public class UsersController : Controller
    {


        string parentid = System.Web.HttpContext.Current.User.Identity.GetUserId();
        ApplicationDbContext db = new ApplicationDbContext();
        private readonly IResellerService _resellerService;
        private readonly IVResellerCustomerService _vResellerCustomerService;
        private readonly ICountryService _countryService;
        private readonly IStateService _stateService;
        private readonly ICustomerService _customerService;
        private readonly IVResellerService _ivresellerService;
        private readonly IAspNetUserService _aspNetUserService;
        private readonly IAspNetUserRoleService _aspNetUserRoleService;
        private readonly IVAspNetUsersService _vAspNetUsersService;
        private readonly IVCustomerService _vcustomerService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private ApplicationUserManager _userManager;

        public UsersController(IUnitOfWorkAsync unitOfWorkAsync, IResellerService resellerService, IVAspNetUsersService vAspNetUsersService, IAspNetUserService aspNetUserService, IAspNetUserRoleService aspNetUserRoleService, IVResellerService vresellerService, IVCustomerService vcustomerService, IVResellerCustomerService vResellerCustomerService, ICustomerService customerService, ICountryService countryService, IStateService stateService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _countryService = countryService;
            _stateService = stateService;
            _resellerService = resellerService;
            _vAspNetUsersService = vAspNetUsersService;
            _aspNetUserService = aspNetUserService;
            _aspNetUserRoleService = aspNetUserRoleService;
            _ivresellerService = vresellerService;
            _vcustomerService = vcustomerService;
            _vResellerCustomerService = vResellerCustomerService;
            _customerService = customerService;
        }

        public UsersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        #region Resellers
        // POST: Companies/Delete/5
        [HttpPost, ActionName("SaveResllers")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[ValidateAntiForgeryToken]
        //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
        public async Task<ActionResult> SaveResllers()
        {

            Reseller reseller = new Reseller();
            int resellerid = 0;

            if (Request.Form["id"] != null)
                resellerid = Convert.ToInt32(Request.Form["id"]);

            if (resellerid > 0)
                reseller = _resellerService.Find(resellerid);

            reseller.Name = Request.Form["name"];
            reseller.Status = Convert.ToBoolean(Convert.ToInt16(Request.Form["status"]));

            if (resellerid > 0)
            {
                //reseller.ObjectState =
                _resellerService.Update(reseller);
            }
            else
            {
                _resellerService.Insert(reseller);
            }

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {

                int i = 0;
                string s = ex.InnerException.ToString();
                s = ex.InnerException.Message;
                s = ex.InnerException.Source;
            }

            Message message = new Message
            {
                Status = true
            };

            return Json(message);

        }

        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public ActionResult ListResllers(bool disable = false, string filter = "")
        {
            var query = from VReseller in _ivresellerService.Queryable() select VReseller;
            if (disable == false) query = query.Where(x => x.Status != disable);

            if (!string.IsNullOrEmpty(filter)) query = query.Where(x => x.Name.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                accounts = x.Accounts,
                leases = x.Leases,
                status = x.Status
            });

            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = LCAdmin.Json)]

        public ActionResult LCReselllers(bool disable = false, string filter = "")
        {
            return View();
        }


        // POST: Companies/Delete/5
        [HttpPost, ActionName("DeleteResllers")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> DeleteConfirmedResllers()
        {
            Reseller reseller = new Reseller();
            int resellerid = 0;

            if (Request.Form["id"] != null)
                resellerid = Convert.ToInt32(Request.Form["id"]);

            if (resellerid > 0)
                reseller = _resellerService.Find(resellerid);
            reseller.Status = false;

            if (resellerid > 0)
            {
                _resellerService.Update(reseller);
            }

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {


            }

            Message message = new Message
            {
                Status = true
            };

            return Json(message);
        }
        #endregion

        #region Customer
        //[HttpGet]
        public ActionResult LCCustomer(int? id)
        {
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            //var roles = roleMngr.Roles.Where(x => x.Name.EndsWith("Customer")).ToList();
            var roles = roleMngr.Roles.ToList();
            ViewBag.roles = new SelectList(roles, "Id", "Name");
            ViewBag.resellerid = id;
            ViewBag.rolesJson = JsonConvert.SerializeObject(roles);
            return View();
        }

        [HttpGet]
        public ActionResult ListResellerCustomer(bool disable = false, string filter = "")
        {
            int customerid = 0;
            if (Session["customerid"] != null) customerid = Convert.ToInt32(Session["customerid"]);

            LeaseCrunchContext data = new LeaseCrunchContext();
            var query = from VCustomer in data.VCustomers select VCustomer;
            query = query.Where(x => x.CustomerId == customerid);
            query = query.Where(x => x.UserLevel == ((int)UserLevel.Customer) || x.UserLevel == ((int)UserLevel.Customer_User) || x.UserLevel == ((int)UserLevel.Customer_Readonly));
            if (!disable)
                query = query.Where(x => x.Status == (int)UserStatus.Active);

            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.UserName.Contains(filter) || x.Email.Contains(filter) || x.FirstName.Contains(filter) || x.LastName.Contains(filter) || x.Reseller.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.FirstName + " " + x.LastName,
                firstname = x.FirstName,
                lastname = x.LastName,
                reseller = x.Reseller,
                leases = x.Leases,
                email = x.Email,
                resellerid = x.ResellerId,
                role = x.Role,
                emailconfirmed = x.EmailConfirmed,
                customer = x.Customer,
                customerid = x.CustomerId,
                status = x.Status
            });
            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region "LC Admin"

        //[HttpGet]
        public ActionResult LCAdmin(bool disable = false, string filter = "")
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.Where(x => x.Id.Equals("8e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b")).ToList();

            var list = new SelectList(roles, "Id", "Name");
            ViewBag.roles = list;

            ViewBag.rolesJson = JsonConvert.SerializeObject(roles);
            return View();
        }

        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ActionResult List(string filter, Boolean userstatus)
        {
            LeaseCrunchContext data = new LeaseCrunchContext();

            //var query = from user in db.Users select user;
            var query = from VAspNetUsers in data.VAspNetUsers select VAspNetUsers;
            query = query.Where(x => x.UserLevel == ((int)UserLevel.LCAdmin) && x.RoleId.Equals("8e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b"));

            if (!userstatus)
                query = query.Where(x => x.Status == (int)UserStatus.Active);

            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.UserName.Contains(filter) || x.Email.Contains(filter) || x.FirstName.Contains(filter) || x.LastName.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.FirstName + " " + x.LastName,
                firstname = x.FirstName,
                lastname = x.LastName,
                email = x.Email,
                role = x.Role,
                status = x.Status,
                emailconfirmed = x.EmailConfirmed ? 1 : 0
            });

            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var applicationUser = _vAspNetUsersService.Find(id);
            _vAspNetUsersService.Delete(applicationUser);
            _unitOfWorkAsync.SaveChanges();
            Message message = new Message
            {
                Status = true
            };
            return Json(message);
        }
        #endregion

        #region "LC Reseller User"
        //[HttpGet]
        public ActionResult LCResellerUsers(int id = 0)
        {
            if (id > 0)
                Session["resellerid"] = id;
          

            ApplicationDbContext db = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.Where(r => r.Id.Equals("7e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b")).ToList();
            var list = new SelectList(roles, "Id", "Name");
            ViewBag.roles = list;

            ViewBag.rolesJson = JsonConvert.SerializeObject(roles);
            ViewBag.resellerid = id;
            return View();
        }

        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ActionResult ListResellerUser(bool disable, string filter, int id = 0)
        {
            LeaseCrunchContext data = new LeaseCrunchContext();

            //var query = from user in db.Users select user;
            var query = from VAspNetUsers in data.VAspNetUsers select VAspNetUsers;
            if (id > 0)
                query = query.Where(x => x.ResellerId == id);
            query = query.Where(x => x.UserLevel == ((int)UserLevel.Reseller));

            if (!disable)
                query = query.Where(x => x.Status == (int)UserStatus.Active);


            if (!string.IsNullOrEmpty(filter))
                query = query.Where(x => x.UserName.Contains(filter) || x.Email.Contains(filter) || x.FirstName.Contains(filter) || x.LastName.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.FirstName + " " + x.LastName,
                firstname = x.FirstName,
                lastname = x.LastName,
                email = x.Email,
                role = x.Role,
                emailconfirmed = x.EmailConfirmed ? 1 : 0
            });

            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult ListCustomerAccount()
        {
            LeaseCrunchContext data = new LeaseCrunchContext();
            var query = from VCustomerAccount in data.VCustomerAccounts select VCustomerAccount;
            var collection = query.Select(x => new
            {
                customerid = x.CustomerId,
                customer = x.Customer,
                resellerid = x.ResellersId,
                reseller = x.Reseller,
                leases = x.Leases
            });
            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        /***********************************************needs to fix it*******************************************/
        #region Reseller Companies
        public ActionResult LCResellerCustomers(bool disable = false, string filter = "")
        {
            return View();
        }
        [HttpGet]
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public ActionResult ListResellerCustomers(bool disable = false, string filter = "")
        {
            var query = from VResellerCustomer in _vResellerCustomerService.Queryable() select VResellerCustomer;
            int resellerid = Convert.ToInt32(Session["resellerid"].ToString());
            query = query.Where(x => x.ResellerId == resellerid);
            if (disable == false) query = query.Where(x => x.Status != disable);

            if (!string.IsNullOrEmpty(filter)) query = query.Where(x => x.Name.Contains(filter));

            var collection = query.Select(x => new
            {
                id = x.Id,
                name = x.Name,
                company = x.company,
                leases = x.Leases,
                status = x.Status
            });

            return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

        }



        // POST: Companies/Delete/5
        [HttpPost, ActionName("SaveResllerCustomer")]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[ValidateAntiForgeryToken]
        //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
        public async Task<ActionResult> SaveResllerCustomer()
        {

            Customer customer = new Customer();
            int customerid = 0;

            if (Request.Form["id"] != null)
                customerid = Convert.ToInt32(Request.Form["id"]);

            if (customerid > 0)
                customer = _customerService.Find(customerid);

            customer.Name = Request.Form["name"];
            customer.Status = Convert.ToBoolean(Convert.ToInt16(Request.Form["status"]));
            customer.ResellerId = Convert.ToInt16(Request.Form["resellerid"]);

            if (customerid > 0)
            {
                //reseller.ObjectState =
                _customerService.Update(customer);
            }
            else
            {
                _customerService.Insert(customer);
            }

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
            }

            Message message = new Message
            {
                Status = true
            };

            return Json(message);

        }


        #endregion

        #region Leases
        public ActionResult LCLeases(bool disable = false, string filter = "")
        {
            return View();
        }
        public ActionResult LCMainLeasesAdmin(bool disable = false, string filter = "")
        {
            return View();
        }
        #endregion

        #region AccountInfo
        public ActionResult AccountInfo()
        {
            IQueryable<Country> countries = _countryService.Queryable();
            Country country = new Country();
            country = countries.Where(c => c.Id == 278).FirstOrDefault();
            ViewBag.CountryId = new SelectList(countries, "Id", "Name", country);
            ViewBag.state = new SelectList(_stateService.Queryable(), "Id", "Name");
            return View();
        }

        [HttpPost, ActionName("PopulateAccountInfo")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public ActionResult PopulateAccountInfo()
        {
            //AccountController a = new AccountController();
            //var aspNetUser = a.UserManager.FindById(parentid);
            //var reseller = _resellerService.Queryable().Where(x => x.Id == aspNetUser.ResellerId).FirstOrDefault();

            var user = UserManager.FindById(User.Identity.GetUserId());
            int resellerid = Convert.ToInt16(Session["resellerid"]);
            var reseller = _resellerService.Queryable().Where(x => x.Id == resellerid).FirstOrDefault();

            Reseller resellerObj = new Reseller()
            {
                Id = reseller.Id,
                Name = reseller.Name,
                Status = reseller.Status,
                BillingEmail = reseller.BillingEmail,
                Country = reseller.Country,
                AddressLine1 = reseller.AddressLine1,
                AddressLine2 = reseller.AddressLine2,
                Zip = reseller.Zip,
                City = reseller.City,
                State = reseller.State
            };
            return Json(resellerObj);
        }



        [HttpPost, ActionName("SaveAccountInfo")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> SaveAccountInfo()
        {
            Message message = new Message()
            {
                Status = true,
                message = ""
            };

            int id = Convert.ToInt16(Request.Form["Id"]);
            Reseller reseller = _resellerService.Find(id);
            // reseller.Id = Convert.ToInt16(Request.Form["resellerid"]);
            reseller.Name = Request.Form["name"];
            reseller.BillingEmail = Request.Form["billingemail"];
            reseller.Country = Convert.ToInt16(Request.Form["Country"]);
            reseller.AddressLine1 = Request.Form["addressline1"];
            reseller.AddressLine2 = Request.Form["addressline1"];
            reseller.Zip = Request.Form["zip"];
            reseller.City = Request.Form["city"];
            reseller.State = Convert.ToInt16(Request.Form["state"]);
            try
            {
                reseller.ObjectState = ObjectState.Modified;
                _resellerService.Update(reseller);
                _unitOfWorkAsync.SaveChanges();

                message.message = "Account Info updated sucessfully";
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.message = "Error Occured!";
            }

            return Json(message);
        }
        #endregion

    }
}