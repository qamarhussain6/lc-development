﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Common;
using LeaseCrunch.Service;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using Repository.Pattern.Infrastructure;
using Microsoft.AspNet.Identity;
using LeaseCrunch.Web.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Reflection;
using Microsoft.AspNet.Identity.EntityFramework;
using LeaseCrunch.Web.Services;
using Newtonsoft.Json;


namespace LeaseCrunch.Web.Controllers
{

    [Authorize]
    public class AccountController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        string parentid = System.Web.HttpContext.Current.User.Identity.GetUserId();

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.ToList();
            ViewBag.roles = new SelectList(roles, "Id", "Name");
            return View();
        }

        #region LCAdmin
        // POST: Companies/Delete/5
        [HttpPost, ActionName("SaveAdmin")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[ValidateAntiForgeryToken]
        //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
        public async Task<ActionResult> SaveAdmin(Guid id)
        {
            Message message = new Message()
            {
                Status = true,
                message = ""
            };

            if (id == Guid.Empty && UserManager.FindByEmail(Request.Form["email"]) != null)
            {
                message.Status = false;
                message.message = "Email address already exists";
                return Json(message);
            }

            if (id == Guid.Empty)
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.UserName = applicationUser.Email = Request.Form["email"];
                //applicationUser.EmailConfirmed = Convert.ToBoolean(Convert.ToByte(Request.Form["emailconfirmed"]));
                applicationUser.EmailConfirmed = false;
                applicationUser.UserLevel = (int)UserLevel.LCAdmin;
                applicationUser.ParentId = null;
                int? resellerid = null;
                applicationUser.ResellerId = resellerid;
                applicationUser.Status = (int)UserStatus.Disabled;
                //IdentityResult result = IdentityResult.Success;
                try
                {
                    var result = UserManager.Create(applicationUser, "P@ssw0rd");
                    if (result.Succeeded)
                    {
                        var roleresult = UserManager.AddToRole(applicationUser.Id, Request.Form["roles"].Trim().Replace('\n', ' '));
                        string code = await UserManager.GenerateEmailConfirmationTokenAsync(applicationUser.Id);
                        //await UserManager.ConfirmEmailAsync(applicationUser.Id, code);
                        string Resetcode = await UserManager.GeneratePasswordResetTokenAsync(applicationUser.Id);
                        var callbackUrl = Url.Action("LCRegister", "Account", new { userId = applicationUser.Id, code = Resetcode }, protocol: Request.Url.Scheme);
                        var messageBody = getUserCreateMessage(applicationUser, callbackUrl);
                        //await EmailService..SendEmail(applicationUser.Id, "LC Crunch Support", messageBody);
                        Email.SendMail("LeaseCrunch Invitation", messageBody, applicationUser.Email);
                        message.Status = true;
                        // await sendEmail(Request.Form["email"], Request.Form["roles"].Trim().Replace('\n', ' '));
                        message.message = "User Invited Sucessfully";
                        return Json(message);
                    }
                    else
                    {
                        message.Status = false;
                        message.message = " Email [" + Request.Form["email"] + "] already exists, the email could not be sent";
                        //message.message = "Unable to Invite Sucessfully";
                        return Json(message);
                    }
                }
                catch (Exception ex)
                {
                    //string s = ex.ToString();
                    message.Status = false;

                    message.message = ex.InnerException.ToString();
                    return Json(message);
                }
            }
            else
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser = db.Users.Find(id.ToString());

                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.Email = Request.Form["email"];
                //applicationUser.EmailConfirmed = Convert.ToBoolean(Convert.ToByte(Request.Form["emailconfirmed"]));
                applicationUser.EmailConfirmed = true;
                applicationUser.Status = Convert.ToInt16(Request.Form["userstatus"]);
                if (applicationUser.Status == (int)UserStatus.Disabled && User.Identity.Name == applicationUser.Email)
                {
                    message.Status = false;
                    message.message = "You can't disable your own account";
                    return Json(message);
                }

                if (applicationUser.Status == (int)UserStatus.Active && CountUserByLevels((UserLevel)applicationUser.UserLevel) <= 1)
                {
                    message.Status = false;
                    message.message = "This user account can't be disabled";
                    return Json(message);
                }

                db.Entry(applicationUser).State = EntityState.Modified;
                db.SaveChanges();

                message.message = "User Updated Sucessfully";
            }

            return Json(message);

        }

        private int CountUserByLevels(UserLevel userLevel)
        {
            int count = db.Users.Where(u => u.UserLevel == (int)userLevel && u.Status == (int)UserStatus.Active).Count();
            return count;
        }


        private string getUserCreateMessage(ApplicationUser user, string callbackUrl)
        {
            StringBuilder email = new StringBuilder();
            var code = UserManager.GeneratePasswordResetToken(user.Id);

            email.AppendLine("Hi " + user.FullName + ",");
            email.AppendLine("<br><br>");
            email.AppendLine("You are invited to create an Admin Account on LeaseCrunch&#46;com.");
            email.AppendLine("<br><br>");
            email.AppendLine("Click on the link below to get started.");
            email.AppendLine("<br><br>");
            email.AppendLine("<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a> ");
            email.AppendLine("<br><br>");
            email.AppendLine("Thanks,");
            email.AppendLine("<br><br>");
            email.AppendLine("LeaseCrunch Support");
            return email.ToString();
        }

        public async Task<ActionResult> sendEmail(string userEmail, string role)
        //private  bool  sendEmail(string userEmail, string role)
        {
            var user = UserManager.FindByName(userEmail);

            var roleresult = UserManager.AddToRole(user.Id, role);
            var code = UserManager.GeneratePasswordResetToken(user.Id);
            var callbackUrl = Url.Action("LCRegister", "Account", new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);

            StringBuilder email = new StringBuilder();
            email.AppendLine("Hi " + Request.Form["firstname"] + " " + Request.Form["lastname"] + ",");
            email.AppendLine("<br><br>");
            email.AppendLine("You are invited to create an Admin Account on LeaseCrunch&#46;com.");
            email.AppendLine("<br><br>");
            email.AppendLine("Click on the link below to get started.");
            email.AppendLine("<br><br>");
            email.AppendLine("<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a> ");
            email.AppendLine("<br><br>");
            email.AppendLine("Thanks,");
            email.AppendLine("<br><br>");
            email.AppendLine("LeaseCrunch Support");
            Services.Email.SendMail("LeaseCrunch Support", email.ToString(), userEmail);
            //Boolean mailSent = MailMessage.SendMail(Request.Form["email"], "confirmation@elasticroi.com", "LC Crunch Support", email.ToString());
            // Boolean mailSent = MailMessage.SendMail();

            return null;
        }

        #endregion

        #region Reseller User
        // POST: Companies/Delete/5
        [HttpPost, ActionName("SaveReseller")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //[ValidateAntiForgeryToken]
        //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
        public async Task<ActionResult> SaveReseller(Guid id)
        {

            Message message = new Message
            {
                Status = true
            };
            string e_mail = Request.Form["email"];
            var user = await UserManager.FindByNameAsync(e_mail);
            if (user == null)
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.UserName = applicationUser.Email = Request.Form["email"];
                applicationUser.ResellerId = Convert.ToInt16(Request.Form["resellerid"]);
                applicationUser.EmailConfirmed = false;
                applicationUser.UserLevel = (int)UserLevel.Reseller;
                applicationUser.ParentId = parentid;
                try
                {
                    var result = await UserManager.CreateAsync(applicationUser, "P@ssw0rd");
                    if (result.Succeeded)
                    {
                        user = await UserManager.FindByNameAsync(e_mail);
                        string role = Request.Form["roles"].Trim().Replace('\n', ' ');
                        var roleresult = UserManager.AddToRole(user.Id, role);
                        var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                        var callbackUrl = Url.Action("LCRegister", "Account", new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);

                        StringBuilder email = new StringBuilder();
                        email.AppendLine("Hi " + Request.Form["firstname"] + " " + Request.Form["lastname"] + ",");
                        email.AppendLine("<br><br>");
                        email.AppendLine("You are invited to create a LeaseCrunch Reseller Account on LeaseCrunch&#46;com.");
                        email.AppendLine("<br><br>");
                        email.AppendLine("Click on the link below to get started.");
                        email.AppendLine("<br><br>");
                        email.AppendLine("<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a> ");
                        email.AppendLine("<br><br>");
                        email.AppendLine("Thanks,");
                        email.AppendLine("<br><br>");
                        email.AppendLine("LeaseCrunch Support");
                        Services.Email.SendMail("LeaseCrunch Invitation", email.ToString(), applicationUser.Email);
                        message.Status = true;
                        message.message = " User Invited Successfully";
                        return Json(message);


                    }
                }
                catch (Exception ex)
                {
                    if (id == Guid.Empty)
                    {
                        message.Status = false;
                        message.message = " Email could not be sent";
                        //message.message = "Unable to Invite Sucessfully";
                        return Json(message);
                    }
                }
            }
            else
            {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser = db.Users.Find(id.ToString());
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.Email = Request.Form["email"];
                applicationUser.Status = Convert.ToInt16(Request.Form["userstatus"]);
                db.Entry(applicationUser).State = EntityState.Modified;

                if (applicationUser.Status == (int)UserStatus.Disabled && User.Identity.Name == applicationUser.Email)
                {
                    message.Status = false;
                    message.message = "You can't disable your own account";
                    return Json(message);
                }

                if (applicationUser.Status == (int)UserStatus.Disabled && CountUserByLevels((UserLevel)applicationUser.UserLevel) <= 1)
                {
                    message.Status = false;
                    message.message = "This user account can't be disabled";
                    return Json(message);
                }

                await db.SaveChangesAsync();
                db.SaveChanges();

                message.Status = true;
                message.message = "User Updated Successfully";

            }
            return Json(message);
        }
    #endregion


    #region Customers

    public ActionResult LCCustomer()
    {
        var roleStore = new RoleStore<IdentityRole>(db);
        var roleMngr = new RoleManager<IdentityRole>(roleStore);
        var roles = roleMngr.Roles.ToList();
        ViewBag.roles = new SelectList(roles, "Id", "Name");
        return View();
    }


    [HttpGet]
    //[WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]

    public ActionResult ListCustomer(bool disable = false, string filter = "")
    {

        LeaseCrunchContext data = new LeaseCrunchContext();

        //var query = from user in db.Users select user;
        var query = from VAspNetUsers in data.VAspNetUsers select VAspNetUsers;
        query = query.Where(x => x.UserLevel == (int)UserLevel.Customer);

        List<VAspNetUser> users = new List<VAspNetUser>();
        if (disable == false)
            query = query.Where(x => x.EmailConfirmed != disable);

        if (!string.IsNullOrEmpty(filter))
            query = query.Where(x => x.UserName.Contains(filter) || x.Email.Contains(filter) || x.FirstName.Contains(filter) || x.LastName.Contains(filter));

        var collection = query.Select(x => new
        {
            id = x.Id,
            name = x.FirstName + " " + x.LastName,
            firstname = x.FirstName,
            lastname = x.LastName,
            email = x.Email,
            role = x.Role,
            status = x.Status
        });

        return Json(new { draw = 1, recordsTotal = collection.Count(), recordsFiltered = 50, data = collection }, JsonRequestBehavior.AllowGet);

    }

    // POST: Companies/Delete/5
    [HttpPost, ActionName("DeleteCustomer")]
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public async Task<ActionResult> DeleteConfirmedCustomer(string id)
    {
        var applicationUser = db.Users.Find(id);
        db.Users.Remove(applicationUser);
        Message message = new Message
        {
            Status = true
        };
        db.SaveChanges();
        return Json(message);

    }


    // POST: Companies/Delete/5
    [HttpPost, ActionName("SaveCustomer")]
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //[ValidateAntiForgeryToken]
    //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
    public async Task<ActionResult> SaveCustomer(Guid id)
    {

        Message message = new Message
        {
            Status = true
        };
        string e_mail = Request.Form["email"];
        var user = await UserManager.FindByNameAsync(e_mail);
        if (user == null)
        {
            ApplicationUser applicationUser = new ApplicationUser();
            applicationUser.FirstName = Request.Form["firstname"];
            applicationUser.LastName = Request.Form["lastname"];
            applicationUser.UserName = applicationUser.Email = Request.Form["email"];
            applicationUser.EmailConfirmed = false;

            if (Session["customerid"] != null)
                applicationUser.CustomerId = Convert.ToInt32(Session["customerid"]);

            UserLevel userLevel = UserLevel.Customer_Readonly;
            string role = Request.Form["roles"].Trim().Replace('\n', ' ');
            string roleName = string.Empty;
            switch (role)
            {
                case "2e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b":   //Customer User
                    userLevel = UserLevel.Customer_User;
                    roleName = "Customer User";
                    break;
                case "3e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b":   //Customer Readonly
                    userLevel = UserLevel.Customer_Readonly;
                    roleName = "Customer Readonly";
                    break;
                case "5e4d9b8e-a27b-4ab7-9a53-c9ccc2b63a9b":  //Customer
                    userLevel = UserLevel.Customer;
                    roleName = "Customer";
                    break;
            }
            applicationUser.UserLevel = (int)userLevel;
            applicationUser.ParentId = parentid;

            var result = await UserManager.CreateAsync(applicationUser, "P@ssw0rd");
            if (result.Succeeded)
            {
                user = await UserManager.FindByNameAsync(e_mail);

                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                var objrole = await RoleManager.FindByIdAsync(role);
                var roleresult = UserManager.AddToRole(user.Id, objrole.Name);
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("LCRegister", "Account", new { UserId = user.Id, code = code }, protocol: Request.Url.Scheme);

                StringBuilder email = new StringBuilder();
                email.AppendLine("Hi " + Request.Form["firstname"] + " " + Request.Form["lastname"] + ",");
                email.AppendLine("<br><br>");
                email.AppendLine("You are invited to create a " + roleName + " account on LeaseCrunch&#46;com.");
                email.AppendLine("<br><br>");
                email.AppendLine("Click on the link below to get started.");
                email.AppendLine("<br><br>");
                email.AppendLine("<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a> ");
                email.AppendLine("<br><br>");
                email.AppendLine("Thanks,");
                email.AppendLine("<br><br>");
                email.AppendLine("LeaseCrunch Support");
                Services.Email.SendMail("LeaseCrunch Invitation", email.ToString(), applicationUser.Email);
                //Boolean mailSent = MailMessage.SendMail(Request.Form["email"], "confirmation@elasticroi.com", "LC Crunch Support", email.ToString());
                //Boolean mailSent = MailMessage.SendMail();
            }
        }
        else
        {
                ApplicationUser applicationUser = new ApplicationUser();
                applicationUser = db.Users.Find(id.ToString());
                applicationUser.FirstName = Request.Form["firstname"];
                applicationUser.LastName = Request.Form["lastname"];
                applicationUser.Email = Request.Form["email"];
                applicationUser.Status = Convert.ToInt16(Request.Form["userstatus"]);
                if (applicationUser.Status == (int)UserStatus.Disabled && User.Identity.Name == applicationUser.Email)
                {
                    message.Status = false;
                    message.message = "You can't disable your own account";
                    return Json(message);
                }
                if (applicationUser.Status == (int)UserStatus.Disabled && applicationUser.UserLevel == (int)UserLevel.Customer  && CountUserByLevels(UserLevel.Customer) <= 1 )
                {
                    message.Status = false;
                    message.message = "This user account can't be disabled";
                    return Json(message);
                }
                db.Entry(applicationUser).State = EntityState.Modified;
                await db.SaveChangesAsync();
                db.SaveChanges();
                message.Status = true;
                message.message = "User Updated Successfully";
            }
        return Json(message);
    }
    #endregion



    //
    // GET: /Account/Login
    [AllowAnonymous]
    public ActionResult Login(string returnUrl)
    {
        ViewBag.ReturnUrl = returnUrl;
        return View();
    }

    //
    // POST: /Account/Login
    [HttpPost]
    [AllowAnonymous]
    //[ValidateAntiForgeryToken]
    public async Task<ActionResult> Login()
    {
        Message message = new Message()
        {
            message = string.Empty,
            Status = true
        };
        // This doesn't count login failures towards account lockout
        // To enable password failures to trigger account lockout, change to shouldLockout: true
        string email = Request.Form["email"];
        string password = Request.Form["password"];
        string returnUrl = Request.Form["returnurl"];
        Boolean isPersistent = true;
        var result = await SignInManager.PasswordSignInAsync(email, password, isPersistent, shouldLockout: false);
        switch (result)
        {
            case SignInStatus.Success:
                {
                    //Redirect the user according to its role
                    var user = await UserManager.FindByNameAsync(email);
                    Boolean isRedirect = false;
                    Session["customerid"] = user.CustomerId;
                        LeaseCrunchContext lc = new LeaseCrunchContext();
                        var reseller = lc.Customers.AsQueryable().Where(x => x.Id == user.CustomerId.Value).FirstOrDefault();
                        if(reseller!=null)
                        {
                            Session["resellerid"] = reseller.ResellerId;
                        }
                      
                        if (user.EmailConfirmed == false) { LogOff(); goto case SignInStatus.Failure; }
                    if (user.Status == (int)UserStatus.Disabled) { LogOff(); goto case SignInStatus.Failure; }
                    string actionName = string.Empty;
                    string routeValues = string.Empty;
                        if (user.UserLevel == (int)UserLevel.LCAdmin)
                        {
                            routeValues = "Users";
                            actionName = "LCAdmin";
                            isRedirect = true;
                        }
                        else if (user.UserLevel == (int)UserLevel.Reseller)
                        {
                            routeValues = "Users";
                            actionName = "LCResellerCustomers";
                            isRedirect = true;
                        }
                        else if ((user.UserLevel == (int)UserLevel.Customer) || (user.UserLevel == (int)UserLevel.Customer_User))
                        {
                            GetCustomer(user);
                            routeValues = "Companies";
                            actionName = "Index";
                            isRedirect = true;
                        }
                        else if (user.UserLevel == (int)UserLevel.Customer_Readonly)
                        {
                            GetCustomer(user);
                            routeValues = "Leases";
                            actionName = "MyLeases";
                            isRedirect = true;
                        }
                    return Json(new
                    {
                        redirectUrl = Url.Action(actionName, routeValues),
                        isRedirect = isRedirect
                    });
                }

            case SignInStatus.LockedOut:
                message.Status = false;
                message.message = "Your account has been locked";
                return Json(message);
            // return View("Lockout");
            case SignInStatus.RequiresVerification:
                message.Status = false;
                message.message = "Your account is still waiting for verification";
                return Json(message);
            //                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = isPersistent });
            case SignInStatus.Failure:
            default:
                //ModelState.AddModelError("", "Invalid login attempt.");
                message.Status = false;
                message.message = "Invalid login attempt.";
                return Json(message);
        }
    }
        private void GetCustomer(ApplicationUser applicationUser)
        {
            LeaseCrunchContext leaseCrunchContext = new LeaseCrunchContext();
            var customer  = leaseCrunchContext.Customers.Where(x => x.Id == applicationUser.CustomerId ).FirstOrDefault();
            if (customer != null)
            {
                Session["customer"] = customer.Name;
            }

        }
    //
    // GET: /Account/VerifyCode
    [AllowAnonymous]
    public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
    {
        // Require that the user has already logged in via username/password or external login
        if (!await SignInManager.HasBeenVerifiedAsync())
        {
            return View("Error");
        }
        return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
    }

    //
    // POST: /Account/VerifyCode
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }

        // The following code protects for brute force attacks against the two factor codes. 
        // If a user enters incorrect codes for a specified amount of time then the user account 
        // will be locked out for a specified amount of time. 
        // You can configure the account lockout settings in IdentityConfig
        var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
        switch (result)
        {
            case SignInStatus.Success:
                return RedirectToLocal(model.ReturnUrl);
            case SignInStatus.LockedOut:
                return View("Lockout");
            case SignInStatus.Failure:
            default:
                ModelState.AddModelError("", "Invalid code.");
                return View(model);
        }
    }
    public ActionResult LCAdmin()
    {
        return View();
    }

    //public ActionResult LCReselllers()
    //{
    //    return View();
    //}

    public ActionResult LCLeases()
    {
        return View();
    }

    //public ActionResult LCCustomer()
    //{
    //    return View();
    //}

    //
    // GET: /Account/Register
    [AllowAnonymous]
    public ActionResult Register()
    {
        return View();
    }

    //
    // POST: /Account/Register
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Register(RegisterViewModel model)
    {
        if (ModelState.IsValid)
        {
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };

            try
            {
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    //await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    //await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    //Custom Code By Abdul rehman
                    await UserManager.ConfirmEmailAsync(user.Id, code);

                    string Resetcode = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = Resetcode }, protocol: Request.Url.Scheme);
                    await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please user.Id your password by clicking <a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>");

                    return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Excption");
            }


            //AddErrors(result);
        }

        // If we got this far, something failed, redisplay form
        return View(model);
    }




    //
    // GET: /Account/ConfirmEmail
    [AllowAnonymous]
    public async Task<ActionResult> ConfirmEmail(string userId, string code)
    {
        if (userId == null || code == null)
        {
            return View("Error");
        }
        var result = await UserManager.ConfirmEmailAsync(userId, code);
        return View(result.Succeeded ? "ConfirmEmail" : "Error");
    }

    //
    // GET: /Account/ForgotPassword
    [AllowAnonymous]
    public ActionResult ForgotPassword()
    {
        return View();
    }

    //
    // POST: /Account/ForgotPassword
    [WebMethod]
    [AllowAnonymous]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //[ValidateAntiForgeryToken]
    [HttpPost, ActionName("SendForgotEmail")]
    public async Task<ActionResult> SendForgotEmail()
    {
        Message message = new Message()
        {
            Status = true,
            message = "Email sent successfully!"
        };
        string email = Request.Form["email"];
        var user = UserManager.FindByName(email);
        if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
        {
            message.Status = false;
            message.message = "User does not exist or is not verified.";
            return Json(message);
        }
        // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        // Send an email with this link
        string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
        //await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append("Dear " + user.FullName + ",");
        stringBuilder.Append("<br><br>");
        stringBuilder.Append("We heard that you lost your LeaseCrunch password. Sorry about that!");
        stringBuilder.Append("<br><br>");
        stringBuilder.Append("But don't worry! You can use the following link within the next day to reset your password:");
        stringBuilder.Append("<br><br>");
        stringBuilder.Append("<a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>");
        stringBuilder.Append("<br><br>");
        stringBuilder.Append("If you don't use this link within 24 hours, it will expire. To get a new password reset link, visit <a href='https://www.leasecrunch.com/ForgotPassword'>https://www.leasecrunch.com/ForgotPassword</a>");
        stringBuilder.Append("<br><br>");
        stringBuilder.Append("Thanks,");
        stringBuilder.Append("<br><br><br>");
        stringBuilder.Append("LeaseCrunch Support");
        try
        {
            Email.SendMail("Reset Password", stringBuilder.ToString(), email);
        }
        catch
        {
            message.Status = false;
            message.message = "Email Not Sent";
            return Json(message);
        }
        //  return RedirectToAction("ForgotPasswordConfirmation", "Account");

        // If we got this far, something failed, redisplay form
        return Json(message);
    }


    private async void CustomSendChangePassword(String userId)
    {
        string code = await UserManager.GeneratePasswordResetTokenAsync(userId);
        var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = userId, code = code }, protocol: Request.Url.Scheme);
        await UserManager.SendEmailAsync(userId, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">" + callbackUrl + "</a>");

        //return RedirectToAction("ForgotPasswordConfirmation", "Account");
    }

    //
    // GET: /Account/ForgotPasswordConfirmation
    [AllowAnonymous]
    public ActionResult ForgotPasswordConfirmation()
    {
        return View();
    }

    //


    // GET: /Account/ResetPasswordConfirmation

    [HttpGet]
    public ActionResult EditProfile()
    {
        return View();
    }
    [HttpPost, ActionName("PopulateProfile")]
    [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ActionResult PopulateProfile()
    {
        LeaseCrunchContext data = new LeaseCrunchContext();

        var aspNetUser = data.AspNetUsers.Where(x => x.Id == parentid).FirstOrDefault();

        Profile profile = new Profile()
        {
            id = aspNetUser.Id,
            email = aspNetUser.Email,
            firstname = aspNetUser.FirstName,
            lastname = aspNetUser.LastName,
        };

        return Json(profile);
    }

    // GET: /Account/ResetPasswordConfirmation
    [HttpPost]
    public async Task<ActionResult> EditProfile(EditProfile model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }
        //var cUser = await UserManager.FindByNameAsync(model.Email);
        ApplicationDbContext context = new ApplicationDbContext();
        UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
        ApplicationUser cUser = await store.FindByNameAsync(model.Email);

        String userId = cUser.Id;
        if (model.Password != string.Empty)
        {
            String newPassword = model.Password; //"<PasswordAsTypedByUser>";
            String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
        }
        cUser.FirstName = model.FirstName;
        cUser.LastName = model.LastName;
        await store.UpdateAsync(cUser);
        return RedirectToAction("EditProfileConfirmation", "Account");
    }

    // POST: Companies/Delete/5
    [HttpPost, ActionName("SaveProfile")]
    [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //[ValidateAntiForgeryToken]
    //[HandleError(View = "AntiForgeryExceptionView", ExceptionType = typeof(HttpAntiForgeryException))]
    public async Task<ActionResult> SaveProfile()
    {
        Message message = new Message()
        {
            Status = true,
            message = ""
        };
        //var cUser = await UserManager.FindByNameAsync(model.Email);
        ApplicationDbContext context = new ApplicationDbContext();
        UserStore<ApplicationUser> store = new UserStore<ApplicationUser>(context);
        ApplicationUser cUser = await store.FindByNameAsync(Request.Form["email"]);
        String userId = cUser.Id;
        String newPassword = Request.Form["password"]; //"<PasswordAsTypedByUser>";
        String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newPassword);
        cUser.FirstName = Request.Form["firstname"];
        cUser.LastName = Request.Form["lastname"];
        try
        {
            await store.SetPasswordHashAsync(cUser, hashedNewPassword);
            await store.UpdateAsync(cUser);
            message.message = "Profile Updated Sucessfully";
        }
        catch (Exception ex)
        {
            message.Status = false;
            message.message = "Error Occured!";
        }

        return Json(message);
    }

    //
    // GET: /Account/ResetPasswordConfirmation
    [AllowAnonymous]
    public ActionResult EditProfileConfirmation()
    {
        return View();
    }


    #region Registration

    // GET: /Account/LCRegister
    [AllowAnonymous]
    public ActionResult LCRegister(string code)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["userId"]))
        {
            var user = UserManager.FindById(Request.QueryString["userId"]);
            ViewBag.Email = user.Email;
        }

        return code == null ? View("Error") : View();
    }

    //
    // POST: /Account/LCRegister
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> LCRegister(ResetPasswordViewModel model)
    {

        if (!ModelState.IsValid)
        {
            return View(model);
        }
        var user = await UserManager.FindByNameAsync(model.Email);
        if (user == null)
        {
            // Don't reveal that the user does not exist
            return RedirectToAction("LCRegisterConfirmation", "Account");
        }
        user.Status = (int)UserStatus.Active;
        user.EmailConfirmed = true;
        UserManager.Update(user);

        var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
        if (result.Succeeded)
        {
            return RedirectToAction("LCRegisterConfirmation", "Account");
        }
        AddErrors(result);
        return View();
    }

    //
    // GET: /Account/LCRegisterConfirmation
    [AllowAnonymous]
    public ActionResult LCRegisterConfirmation()
    {
        return RedirectToAction("Login", "Account");
        //return View();
    }
    #endregion

    // GET: /Account/ResetPassword
    [AllowAnonymous]
    public ActionResult ResetPassword(string code)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["userId"]))
        {
            var user = UserManager.FindById(Request.QueryString["userId"]);
            ViewBag.Email = user.Email;
        }
        return code == null ? View("Error") : View();
    }

    //
    // POST: /Account/ResetPassword
    [HttpPost]
    [AllowAnonymous]
    //[ValidateAntiForgeryToken]
    public async Task<ActionResult> ResetPassword()
    {
        Message message = new Message()
        {
            Status = true,
            message = "Password has been reset"
        };

        string email = Request.Form["email"];
        string password = Request.Form["password"];
        string code = Request.Form["code"];
        var user = await UserManager.FindByNameAsync(email);
        if (user == null)
        {
            message.Status = false;
            message.message = "User does not exist";
        }
        var result = await UserManager.ResetPasswordAsync(user.Id, code, password);
        if (result.Succeeded)
        {
            message.Status = true;
            message.message = "Password has been reset";
        }
        AddErrors(result);
        return Json(message);
    }

    //
    // GET: /Account/ResetPasswordConfirmation
    [AllowAnonymous]
    public ActionResult ResetPasswordConfirmation()
    {
        return View();
    }

    //
    // POST: /Account/ExternalLogin
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult ExternalLogin(string provider, string returnUrl)
    {
        // Request a redirect to the external login provider
        return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
    }

    //
    // GET: /Account/SendCode
    [AllowAnonymous]
    public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
    {
        var userId = await SignInManager.GetVerifiedUserIdAsync();
        if (userId == null)
        {
            return View("Error");
        }
        var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
        var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
        return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
    }

    //
    // POST: /Account/SendCode
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> SendCode(SendCodeViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View();
        }

        // Generate the token and send it
        if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
        {
            return View("Error");
        }
        return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
    }

    //
    // GET: /Account/ExternalLoginCallback
    [AllowAnonymous]
    public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
    {
        var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
        if (loginInfo == null)
        {
            return RedirectToAction("Login");
        }

        // Sign in the user with this external login provider if the user already has a login
        var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
        switch (result)
        {
            case SignInStatus.Success:
                return RedirectToLocal(returnUrl);
            case SignInStatus.LockedOut:
                return View("Lockout");
            case SignInStatus.RequiresVerification:
                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
            case SignInStatus.Failure:
            default:
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
        }
    }

    //
    // POST: /Account/ExternalLoginConfirmation
    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
    {
        if (User.Identity.IsAuthenticated)
        {
            return RedirectToAction("Index", "Manage");
        }

        if (ModelState.IsValid)
        {
            // Get the information about the user from the external login provider
            var info = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return View("ExternalLoginFailure");
            }
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            var result = await UserManager.CreateAsync(user);
            if (result.Succeeded)
            {
                result = await UserManager.AddLoginAsync(user.Id, info.Login);
                if (result.Succeeded)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                    return RedirectToLocal(returnUrl);
                }
            }
            AddErrors(result);
        }

        ViewBag.ReturnUrl = returnUrl;
        return View(model);
    }

    //

    [AllowAnonymous]
    public ActionResult LogOff()
    {
        AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        return RedirectToAction("Index", "Home");
    }

    //
    // GET: /Account/ExternalLoginFailure
    [AllowAnonymous]
    public ActionResult ExternalLoginFailure()
    {
        return View();
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (_userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            if (_signInManager != null)
            {
                _signInManager.Dispose();
                _signInManager = null;
            }
        }

        base.Dispose(disposing);
    }

    #region Helpers
    // Used for XSRF protection when adding external logins
    private const string XsrfKey = "XsrfId";

    private IAuthenticationManager AuthenticationManager
    {
        get
        {
            return HttpContext.GetOwinContext().Authentication;
        }
    }

    private void AddErrors(IdentityResult result)
    {
        foreach (var error in result.Errors)
        {
            ModelState.AddModelError("", error);
        }
    }

    private ActionResult RedirectToLocal(string returnUrl)
    {
        if (Url.IsLocalUrl(returnUrl))
        {
            return Redirect(returnUrl);
        }
        return RedirectToAction("Index", "Home");
    }

    internal class ChallengeResult : HttpUnauthorizedResult
    {
        public ChallengeResult(string provider, string redirectUri)
            : this(provider, redirectUri, null)
        {
        }

        public ChallengeResult(string provider, string redirectUri, string userId)
        {
            LoginProvider = provider;
            RedirectUri = redirectUri;
            UserId = userId;
        }

        public string LoginProvider { get; set; }
        public string RedirectUri { get; set; }
        public string UserId { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
            if (UserId != null)
            {
                properties.Dictionary[XsrfKey] = UserId;
            }
            context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
        }
    }
    #endregion
}
}