﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using LeaseCrunch.Common;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Validation;
using LeaseCrunch.Web.Models;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Net.Mail;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using LeaseCrunch.Web.NavigationAttribute;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Repository.Pattern.Infrastructure;

namespace LeaseCrunch.Web.Controllers
{
    public class CompanyAccountController : Controller
    {
        private readonly ICompanyAccountService _companyAccountService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public CompanyAccountController(IUnitOfWorkAsync unitOfWorkAsync, ICompanyAccountService companyAccountService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _companyAccountService = companyAccountService;
        }

        [HttpPost, ActionName("SaveLease")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> SaveLease()
        {
            Message message = new Message()
            {
                Status = true,
                message = ""
            };
            CompanyAccount companyAccounts = new CompanyAccount();
            companyAccounts.CompanyId = Convert.ToInt32(Request.Form["company"]);
            companyAccounts.CustomerId = Convert.ToInt32(Request.Form["customerid"]);
            //companyAccounts.LeaseType = Convert.ToInt32(Request.Form["leases_operating_leases"]);
            companyAccounts.GLAccountType = Convert.ToInt32(Request.Form["Leases_Operating_Leases"]);
            companyAccounts.CustomerGLDescription = Request.Form["CustomerGLDescription"];
            companyAccounts.GLNumber = Request.Form["GLNumber"];
            companyAccounts.Classification = Convert.ToInt32(Request.Form["Classification"]);
            companyAccounts.FinancialStatement = Convert.ToInt32(Request.Form["FinancialStatement"]);
            string btnid = Request.Form["btnid"];
            companyAccounts.NonLease = Convert.ToBoolean(Convert.ToInt16(btnid.Substring(4, 1)));
            companyAccounts.FinanceLeases = !Convert.ToBoolean(Convert.ToInt16(btnid.Substring(6, 1)));
            try
            {
                _companyAccountService.Insert(companyAccounts);

                _unitOfWorkAsync.SaveChanges();
                message.Status = true;
                message.message = "Saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                message.Status = false;
                message.message = "The record could not be saved";
            }
            return Json(message);
        }



        [HttpPost, ActionName("UpdateLease")]
        [WebMethod]
        [Authorize]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public async Task<ActionResult> UpdateLease()
        {
            Message message = new Message()
            {
                Status = true,
                message = ""
            };
            int id = 0;
            CompanyAccount companyAccounts = new CompanyAccount();
            if (!string.IsNullOrEmpty(Request.Form["id"]))
            {
                id = Convert.ToInt32(Request.Form["id"]);
                companyAccounts = _companyAccountService.Find(id);
            }
            if (!string.IsNullOrEmpty(Request.Form["CustomerGLDescription"])) companyAccounts.CustomerGLDescription = Request.Form["CustomerGLDescription"];
            if (!string.IsNullOrEmpty(Request.Form["GLNumber"])) companyAccounts.GLNumber = Request.Form["GLNumber"];
            try
            {
                _companyAccountService.Update(companyAccounts);
                companyAccounts.ObjectState = ObjectState.Modified;

                _unitOfWorkAsync.SaveChanges();
                message.Status = true;
                message.message = "Saved successfully";
            }

            catch (Exception ex)
            {
                message.Status = false;
                message.message = "The record could not be saved";
            }
            return Json(message);
        }

        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult CashAP(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && l.GLAccountType == 31);

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult ROUAsset(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && (l.GLAccountType == 8 || l.GLAccountType == 13));

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult LTLeaseLiability(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && (l.GLAccountType == 9 || l.GLAccountType == 14));

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }

        

        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult STLeaseLiability(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && (l.GLAccountType == 10 || l.GLAccountType == 15));

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult RentExpense(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && l.GLAccountType == 11);

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public async Task<ActionResult> List()
        public ActionResult GLAccountTypeNonLease(int CompanyId = 0)
        {
            var location = _companyAccountService.Queryable().Where(l => l.CompanyId == CompanyId && l.NonLease == true);

            var collection = location.Select(x => new
            {
                id = x.Id,
                customergldescription = x.CustomerGLDescription,
                glnumber = x.GLNumber
            }).ToList();

            return Json(collection, JsonRequestBehavior.AllowGet);
        }



    }
}