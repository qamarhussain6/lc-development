﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using LeaseCrunch.Common;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.Entity.Validation;
using LeaseCrunch.Web.Models;
using Microsoft.AspNet.Identity;
using System.Text;
using System.Net.Mail;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using LeaseCrunch.Web.NavigationAttribute;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Repository.Pattern.Infrastructure;
using System.IO;

namespace LeaseCrunch.Web.Controllers
{
    public class LeasesController : Controller
    {
        #region private variables
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;
        private readonly ICompanyService _companiesService;
        private readonly ICountryService _countryService;
        private readonly IAccountingStandardService _accountingStandardService;
        private readonly ILeasesService _leasesService;
        private readonly ICustomizationLessorService _customizationLessorService;
        private readonly IStateService _stateService;
        private readonly ICustomizationAssetTypeService _customizationAssetTypesService;
        private readonly ILocationService _locationService;
        private readonly ILessersService _lessersService;
        private readonly ICustomizationCostCenterService _customizationCostCenterService;
        private readonly ICustomizationCustomFieldService _customizationCustomFieldService;
        private readonly ICustomizationCustomFieldsValuesService _customizationCustomFieldsValuesService;
        private readonly IClassificationService _classificationService;
        private readonly ICompanyAccountService _companyAccountService;
        private readonly INonLeasePaymentsProvisionalService _nonLeasePaymentService;
        private readonly ILeasePaymentsProvisionalService _leasePaymentService;
        private readonly ILeaseDocumentsService _leaseDocumentService;
        private readonly ILeaseGLService _leaseGLService;
        #endregion
        // GET: Leases

        #region variables intialization
        public LeasesController(IUnitOfWorkAsync unitOfWorkAsync, CompanyService companiesService, ICustomizationAssetTypeService customizationAssetTypesService, ILocationService locationService,
            ICustomizationCostCenterService customizationCostCenterService, ICustomizationCustomFieldService customizationCustomFieldService, CountryService countryService, StateService stateService,
            AccountingStandardService accountingStandardService, CustomizationLessorService customizationLessorService, LeasesService leasesService, LessersService lessersService, ICustomizationCustomFieldsValuesService customizationCustomFieldsValuesService,
            IClassificationService classificationService, ICompanyAccountService companyAccountService,
            INonLeasePaymentsProvisionalService nonLeasePayment, ILeasePaymentsProvisionalService leasePayment,
            ILeaseDocumentsService leaseDocument,
            ILeaseGLService leaseGl
            )
        {
            _customizationLessorService = customizationLessorService;
            _unitOfWorkAsync = unitOfWorkAsync;
            _companiesService = companiesService;
            _accountingStandardService = accountingStandardService;
            _stateService = stateService;
            _countryService = countryService;
            _locationService = locationService;
            _customizationCostCenterService = customizationCostCenterService;
            _customizationCustomFieldService = customizationCustomFieldService;
            _customizationAssetTypesService = customizationAssetTypesService;
            _leasesService = leasesService;
            _lessersService = lessersService;
            _customizationCustomFieldsValuesService = customizationCustomFieldsValuesService;
            _classificationService = classificationService;
            _companyAccountService = companyAccountService;
            _nonLeasePaymentService = nonLeasePayment;
            _leasePaymentService = leasePayment;
            _leaseDocumentService = leaseDocument;
            _leaseGLService = leaseGl;
        }
        #endregion

        #region CRUD Operations
        public ActionResult MyLeases()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AddLeases()
        {
            LeaseViewModel model = new LeaseViewModel();
            AddLeaseInformation addLeaseInformation = new AddLeaseInformation();
            addLeaseInformation.BeginDate = DateTime.Now;
            addLeaseInformation.EndDate = DateTime.Now.AddYears(1);

            LeasePaymentClassifciationModels leasePaymentClassifciationModels = new LeasePaymentClassifciationModels();
            model.AddLeaseInformation = addLeaseInformation;
            model.leasePayments = new LeasePaymentsProvisionalViewModel();
            model.nonLeasePayments = new NonLeasePaymentsProvisionalViewModel();

            PopulateRecor();
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AddLeases(AddLeaseInformation AddLeaseInformation)
        public ActionResult AddLeases(AddLeaseInformation AddLeaseInformation)
        {
            decimal id = 0;
            PopulateRecor();

            Lease Leases = new Lease();
            if (AddLeaseInformation.Id != null)
            {
                Leases = _leasesService.Find(AddLeaseInformation.Id);
            }
            Leases.CompanyId = AddLeaseInformation.CompanyId;
            Leases.Title = AddLeaseInformation.Title;
            Leases.Description = AddLeaseInformation.Description;
            Leases.Manufacturer = AddLeaseInformation.Manufacturer;
            Leases.AssetTypeId = AddLeaseInformation.AssetTypeId;
            Leases.Size = AddLeaseInformation.Size;
            Leases.CompanyLocationId = AddLeaseInformation.CompanyLocationId;
            Leases.LesserId = AddLeaseInformation.LesserId ;
            Leases.BeginDate = AddLeaseInformation.BeginDate;
            Leases.EndDate = AddLeaseInformation.EndDate;
            Leases.ROUAssetLife = AddLeaseInformation.ROUAssetLife;
            Leases.Terms = AddLeaseInformation.Terms;
            Leases.Comments = AddLeaseInformation.Comments;
            Leases.CostCenterId = AddLeaseInformation.CostCenterId;


            Leases.PaymentFrequencyId = 1;
            Leases.ClassificationId = 1;
            Leases.DiscountRate = 0;
            Leases.IncentiveReceived = 0;
            Leases.InitialDirectCost = 0;
            Leases.Status = true;
            IList<CustomizationCustomField> customizationCustomFields = new List<CustomizationCustomField>();
            int CustomerId = Convert.ToInt32(@Session["customerid"].ToString());
            customizationCustomFields = _customizationCustomFieldService.Queryable().Where(x => x.CustomerId == CustomerId).ToList();
            foreach(var customizationCustomField in customizationCustomFields)
            {
                string ids = "custom_" + customizationCustomField.Id;
                if (!string.IsNullOrEmpty(Request.Form[ids]))
                {
                    CustomizationCustomFieldsValue customizationCustomFieldsValue = new CustomizationCustomFieldsValue();
                    customizationCustomFieldsValue.Lease = Leases;
                    customizationCustomFieldsValue.CustomizationCustomFieldsId = customizationCustomField.Id;
                    customizationCustomFieldsValue.value = Request.Form[ids].ToString();
                    Leases.CustomizationCustomFieldsValues.Add(customizationCustomFieldsValue);
                    _customizationCustomFieldsValuesService.Insert(customizationCustomFieldsValue);
                }
            }
            if (AddLeaseInformation.Id == null)
                _leasesService.Insert(Leases);
            else
            {
                Leases.ObjectState = ObjectState.Modified;
                _leasesService.Update(Leases);
            }
            try
            {
               _unitOfWorkAsync.SaveChanges();
                id = Leases.Id;
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                string r = error;
                //throw;

                ModelState.Remove("Id");

            }

            TempData["LeaseId"] = id;
            //return View(AddLeaseInformation);
            return Json(new { Status = "0", Message = "Lease Added Sucessfully!", Id = id }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddLeaseGL(LeaseGLAccountsViewModel model)
        {
            try
            {
                decimal leaseid =Convert.ToDecimal(Request.Form["GLAccountsLeaseId"]);
                LeaseGLAccount accountModel = new LeaseGLAccount();
                accountModel.AmortExpense = model.AmortExpense;
                accountModel.CashAP = model.CashAP;
                accountModel.GLDescription = model.GLDescription;
                accountModel.InterestExpense = model.InterestExpense;
                accountModel.LeaseId = leaseid;
                accountModel.LTLeaseLiability = model.LTLeaseLiability;
                accountModel.OtherBalanceSheetAccount = model.OtherBalanceSheetAccount;
                accountModel.OtherPLAccount = model.OtherPLAccount;
                accountModel.PaymentFrequency = model.PaymentFrequency;
                accountModel.RentExpense = model.RentExpense;
                accountModel.ROUAsset = model.ROUAsset;
                accountModel.STLeaseLiability = model.STLeaseLiability;
                accountModel.VariableRentExpense = model.VariableRentExpense;
                //retriving and deleting existingRecord incase of update.
                var existingGl = _leaseGLService.Queryable().Where(x => x.LeaseId == leaseid).ToList();
                if(existingGl.Count!=0)
                {
                    foreach(var item in existingGl)
                    {
                        _leaseGLService.Delete(item);
                    }
                }
                //end 
                _leaseGLService.Insert(accountModel);
                _unitOfWorkAsync.SaveChanges();
                return Json(new { Status = "0", Message = "Lease GL Accounts added Sucessfully." }, JsonRequestBehavior.AllowGet);
            }
             catch(Exception ex)
            {
                return Json(new { Status = "0", Message = "Some error occured." }, JsonRequestBehavior.AllowGet);
            }

           
        }

        #region add non lease payments
        public ActionResult AddNonLeasePayments(NonLeasePaymentsProvisionalViewModel model)
        {
            
            try
            {
                var insertedDate = InsertNonLeasePayments(model.PaymentFrequencyNonLease);
              
                return Json(new { Status = "0", Message = "Lease Payments Added Sucessfully!", Id = insertedDate.FirstOrDefault().Id }, JsonRequestBehavior.AllowGet);
            }
           catch(Exception ex)
            {

            }
            return View();
        }

        private List<NonLeasePaymentsProvisional> InsertNonLeasePayments(int PaymentFrequency)
        {
            List<NonLeasePaymentsProvisional> leasePayments = new List<NonLeasePaymentsProvisional>();
            int GridCount = Convert.ToInt32(Request.Form["GridCount"]);
            decimal LeaseId = Convert.ToInt32(Request.Form["NonLeasePaymentLeaseId"]);

            var nonLeasePaymentKeys = new List<string>();
            foreach (var k in Request.Form.Keys)
            {
                if (k.ToString().Contains("PaymentFrequencyNonLease"))
                    nonLeasePaymentKeys.Add(k.ToString());
            }
            List<NonLeasePaymentsProvisional> list = new List<NonLeasePaymentsProvisional>();
            foreach (var outerKey in nonLeasePaymentKeys)
            {
                //var outerIndex = outerKey.Replace("PaymentFrequencyNonLease", "");
                var outerIndex = string.Empty;
                if (outerKey== "PaymentFrequencyNonLease")
                     outerIndex = outerKey.Replace("PaymentFrequencyNonLease", "");
                else
                    outerIndex = outerKey.Replace(outerKey, outerKey.Substring(24, 1));

                var gridKeysList = new List<string>();
                foreach (var k in Request.Form.Keys)
                {
                    if (k.ToString().Contains("paymentstart"))
                        gridKeysList.Add(k.ToString());
                }
               
                //getting first grid
                foreach (var Key in gridKeysList)
                {
                    var index = "0";
                    int payment = 0;
                    int pay = 0;
                    DateTime startDate = DateTime.Now;
                    DateTime endDate = DateTime.Now;

                    index = Key.Replace("][paymentstart]", "");
                    index = index.Replace("det" + outerIndex + "[", "");
                    if (Request.Form.AllKeys.Any(k => k == "det" + outerIndex + "[" + index + "][paymentstart]"))
                    {
                        try
                        {
                            NonLeasePaymentsProvisional leaseP = new NonLeasePaymentsProvisional();

                            if (!string.IsNullOrEmpty(Request.Form["det" + outerIndex + "[" + index + "][paymentamount]"]))
                                payment = Convert.ToInt32(Request.Form["det" + outerIndex + "[" + index + "][paymentamount]"].Trim());

                            if (!string.IsNullOrEmpty(Request.Form["det" + outerIndex + "[" + index + "][payments]"]))
                                pay = Convert.ToInt32(Request.Form["det" + outerIndex + "[" + index + "][payments]"].Trim());

                            if (!string.IsNullOrEmpty(Request.Form["det" + outerIndex + "[" + index + "][paymentstart]"]))
                                startDate = Convert.ToDateTime(Request.Form["det[" + index + "][paymentstart]"]);

                            if (!string.IsNullOrEmpty(Request.Form["det" + outerIndex + "[" + index + "][paymentend]"]))
                                endDate = Convert.ToDateTime(Request.Form["det" + outerIndex + "[" + index + "][paymentend]"]);

                            //leaseP.CompanyAccountId =Convert.ToInt32(Request.Form["GL" + outerKey]);
                            leaseP.PaymentFrequencyId = Convert.ToInt32(Request.Form[outerKey]);
                            leaseP.CompanyAccountId = Convert.ToInt32(Request.Form["GLAccountTypeNonLease"+ outerIndex]);
                            // Request.Form[outerKey];
                            // 

                            leaseP.LeaseId = LeaseId;
                            leaseP.Amount = payment;
                            leaseP.StartDate = startDate;
                            leaseP.EndDate = endDate;
                            list.Add(leaseP);
                           

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                }
            //retriving existing record and deleting in case of update
            var existingRecord = _nonLeasePaymentService.Queryable().Where(x => x.LeaseId == LeaseId).ToList();
            if(existingRecord.Count!=0)
            {
                foreach (var entity in existingRecord)
                {
                    _nonLeasePaymentService.Delete(entity);
                }
            }
            
            //end
            _nonLeasePaymentService.InsertRange(list);
            _unitOfWorkAsync.SaveChanges();
            return list;
        }

        

        #endregion

        #region add lease payments
        [HttpPost]
        public ActionResult AddLeasePayments (LeasePaymentsProvisionalViewModel model)
        {
            var insertedDate=InsertLeasePayments(model.PaymentFrequency);
            return Json(new { Status = "0", Message = "Lease Payments Added Sucessfully!", Id = insertedDate.FirstOrDefault().Id }, JsonRequestBehavior.AllowGet);
        }

        private List<LeasePaymentsProvisional> InsertLeasePayments(int PaymentFrequency)
        {
            List<LeasePaymentsProvisional> leasePayments = new List<LeasePaymentsProvisional>();
            int LeaseId = Convert.ToInt32(Request.Form["LeasePaymentLeaseId"]);
            var gridKeysList = new List<string>();
            foreach (var k in Request.Form.Keys)
            {
                if (k.ToString().Contains("[payamt]"))
                    gridKeysList.Add(k.ToString());
            }
            List<LeasePaymentsProvisional> list = new List<LeasePaymentsProvisional>();
            foreach (var Key in gridKeysList)
            {
                var index = "0";
                int payment = 0;
                int pay = 0;
                DateTime startDate = DateTime.Now;
                index = Key.Replace("][payamt]", "");
                index = index.Replace("det[", "");
                if (Request.Form.AllKeys.Any(k => k == "det[" + index + "][payamt]"))
                {
                    try
                    {
                        LeasePaymentsProvisional leaseP = new LeasePaymentsProvisional();

                        if (!string.IsNullOrEmpty(Request.Form["det[" + index + "][payamt]"]))
                            payment = Convert.ToInt32(Request.Form["det[" + index + "][payamt]"].Trim());

                        if (!string.IsNullOrEmpty(Request.Form["det[" + index + "][pay]"]))
                            pay = Convert.ToInt32(Request.Form["det[" + index + "][pay]"].Trim());

                        if (!string.IsNullOrEmpty(Request.Form["det[" + index + "][paystart]"]))
                            startDate = Convert.ToDateTime(Request.Form["det[" + index + "][paystart]"]);

                        leaseP.LeaseId = LeaseId;
                        leaseP.Amount = payment;
                        leaseP.StartDate = startDate;
                        leaseP.PaymentFrequencyId = PaymentFrequency;

                        if (PaymentFrequency == (int)LeaseCrunch.Common.PaymentFrequency.Monthly)
                        {
                            payment = payment - 1;
                            leaseP.EndDate = startDate.AddMonths(payment);
                        }
                        else if (PaymentFrequency == (int)LeaseCrunch.Common.PaymentFrequency.Quarterly)
                        {
                            payment = payment - 1;
                            leaseP.EndDate = startDate.AddYears(payment);
                        }
                        else if (PaymentFrequency == (int)LeaseCrunch.Common.PaymentFrequency.Yearly)
                        {
                            payment = (payment - 1) * 3;
                            leaseP.EndDate = startDate.AddMonths(payment);
                        }
                        list.Add(leaseP);
                        //_leasePaymentService.Insert(leaseP);
                    }
                    catch (Exception ex)
                    {

                    }

                }
            }
            //retriving and deleting data incase of update payments.
            var existingModel = _leasePaymentService.Queryable().Where(x=>x.LeaseId== LeaseId).ToList();
            if(existingModel.Count != 0)
            {
                foreach(var entity in existingModel)
                {
                    _leasePaymentService.Delete(entity);
                }
                
            }
            //end
            _leasePaymentService.InsertRange(list);
            _unitOfWorkAsync.SaveChanges();
            return list;
        }
        #endregion

        #region uploading lease documents
        public JsonResult Upload()
        {
            int LeaseId = Convert.ToInt32(Request.Form["DocumentLeaseId"]);
            if (Request.Files.Count>0)
            {
                try
                {
                    List<LeaseDocument> list = new List<LeaseDocument>();
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i]; //Uploaded file
                                                                    //Use the following properties to get file's name, size and MIMEType
                        int fileSize = file.ContentLength;
                        string fileName = file.FileName;
                        string mimeType = file.ContentType;
                        System.IO.Stream fileContent = file.InputStream;
                        //To save file, use SaveAs method
                        var path = Path.Combine(Server.MapPath("~/UploadedFiles"), fileName);
                        file.SaveAs(path);
                        //file.SaveAs(Server.MapPath("~/UploadedFiles") + fileName); //File will be saved in application root
                        //saving file info in database
                        var model = new LeaseDocument();
                        model.LeaseId = LeaseId;
                        model.Name = fileName;
                        model.Path = path;
                        FileInfo fi = new FileInfo(path);
                        model.Size = fileSize;
                        model.UploadDate = DateTime.Now;
                        list.Add(model);
                    }
                    //retriving and deleting existing incase of update.
                    var existingDoc = _leaseDocumentService.Queryable().Where(x => x.LeaseId == LeaseId).ToList();
                    if(existingDoc.Count!=0)
                    {
                        foreach(var item in existingDoc)
                        {
                            _leaseDocumentService.Delete(item);
                        }
                    }
                    //end
                    _leaseDocumentService.InsertRange(list);
                    _unitOfWorkAsync.SaveChanges();
                    return Json(list, JsonRequestBehavior.AllowGet);
                }
                catch(Exception ex)
                {
                    return Json("error" + Request.Files.Count + " files");
                }
               
                
            }
            else
            {
                return Json("error" + Request.Files.Count + " files");
            }
           
        }
        #endregion



        #endregion

        //getting GL accounts based on selected clasification
        public ActionResult GetGlAccounts(CompanyAccount model)
        {
            model.CustomerId = Convert.ToInt32(Session["customerid"]);
            if (model.GLAccountType==1)
            {
                ViewBag.AccountsType = "Finance Lease";
                #region cash payble dropdown
                var cashP = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 12 && x.FinanceLeases == true).ToList();
                List<object> newList = new List<object>();
                foreach (var member in cashP)
                    newList.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.CashAccountPayble = new SelectList(newList, "Id", "Name");
                #endregion
                #region Rou Assets dropdown
                var rou = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 13 && x.FinanceLeases == true).ToList();
                List<object> newList1 = new List<object>();
                foreach (var member in rou)
                    newList1.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.RouAsset = new SelectList(newList1, "Id", "Name");
                #endregion
                #region LT Lease libility
                var ltLeaseAb = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 14 && x.FinanceLeases == true).ToList();
                List<object> newList2 = new List<object>();
                foreach (var member in ltLeaseAb)
                    newList2.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.LTLeaseLiab = new SelectList(newList2, "Id", "Name");
                #endregion
                #region ST lease libility
                var stLib = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 15 && x.FinanceLeases == true).ToList();
                List<object> newList3 = new List<object>();
                foreach (var member in stLib)
                    newList3.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.STLeaseLiab = new SelectList(newList3, "Id", "Name");
                #endregion
                #region amartization exp
                var amartionzationExp = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 16 && x.FinanceLeases == true).ToList();
                List<object> newList4 = new List<object>();
                foreach (var member in amartionzationExp)
                    newList4.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.AmortizationExp = new SelectList(newList4, "Id", "Name");
                #endregion
                #region interest exp
                var interestExp = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 5 && x.GLAccountType == 17 && x.FinanceLeases == true).ToList();
                List<object> newList5 = new List<object>();
                foreach (var member in interestExp)
                    newList5.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.InterestExp = new SelectList(newList5, "Id", "Name");
                #endregion
                return PartialView("_FinanceGLAccounts");
            }
            else
            {
                ViewBag.AccountsType = "Operating Lease";
                #region cash account payble
                var cashAccPaybl = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 4 && x.GLAccountType == 31 && x.FinanceLeases == false).ToList();
                List<object> newList6 = new List<object>();
                foreach (var member in cashAccPaybl)
                    newList6.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.CashAccountPayble = new SelectList(newList6, "Id", "Name");
                #endregion
                #region Rou Assets
                var rouAsst = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 4 && x.GLAccountType == 8 && x.FinanceLeases == false).ToList().ToList();
                List<object> newList7 = new List<object>();
                foreach (var member in rouAsst)
                    newList7.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.RouAsset = new SelectList(newList7, "Id", "Name");
                #endregion
                #region lt liease lib
                var ltLeaseLib = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 4 && x.GLAccountType == 9 && x.FinanceLeases == false).ToList().ToList();
                List<object> newList8 = new List<object>();
                foreach (var member in ltLeaseLib)
                    newList8.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.LTLeaseLiab = new SelectList(newList8, "Id", "Name");
                #endregion
                #region st lease libility
                var stLeaseLib = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 4 && x.GLAccountType == 10 && x.FinanceLeases == false).ToList().ToList();
                List<object> newList9 = new List<object>();
                foreach (var member in stLeaseLib)
                    newList9.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.STLeaseLiab = new SelectList(newList9, "Id", "Name");
                #endregion
                #region rent exp
                var rentExpense = _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.CompanyId == model.CompanyId && x.AccountType.ParentId == 4 && x.GLAccountType == 11 && x.FinanceLeases == false).ToList().ToList();
                List<object> newLis = new List<object>();
                foreach (var member in rentExpense)
                    newLis.Add(new
                    {
                        Id = member.Id,
                        Name = member.CustomerGLDescription + " " + member.GLNumber
                    });
                ViewBag.RentExp = new SelectList(newLis, "Id", "Name");
                #endregion
                return PartialView("_OperatingGLAccounts");
            }

            var data= _companyAccountService.Queryable().Where(x => x.CustomerId == model.CustomerId && x.FinanceLeases==true).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //end


        private void PopulateRecor()
        {
            int customerid = 0;
            if (Session["customerid"] != null) customerid = Convert.ToInt32(Session["customerid"]);
            IList<Company> o = new List<Company>();
            ViewBag.CompanyId = new SelectList(o, "Id", "Name");
            List<Location> location = new List<Location>();
            //ViewBag.Location = new SelectList(_locationService.Queryable().ToList(), "Id", "LocationName");
            ViewBag.RentExpense = ViewBag.GLAccountTypeNonLease = ViewBag.STLeaseLiability = ViewBag.LTLeaseLiability= ViewBag.ROUAsset = ViewBag.CashAP = ViewBag.CompanyLocationId = new SelectList(location, "Id", "LocationName");
            ViewBag.Classification = new SelectList(_classificationService.Queryable().ToList(), "Id", "Name"); 
            ViewBag.CostCenter = new SelectList(_customizationCostCenterService.Queryable().Where(x => x.CustomerId == customerid).ToList(), "Id", "Name");
            ViewBag.AssetTypeId = new SelectList(_customizationAssetTypesService.Queryable().Where(x => x.CustomerId == customerid).ToList(), "Id", "Name");
            ViewBag.CustomFields = _customizationCustomFieldService.Queryable().Where(x => x.CustomerId == customerid).ToList();
            ViewBag.LesserId = new SelectList(_lessersService.Queryable().ToList(), "Id", "Name");
            ViewBag.CostCenterId = new SelectList(_customizationCostCenterService.Queryable().Where(x => x.CustomerId == customerid).ToList(), "Id", "Name");
            ViewBag.countryidl = new SelectList(_countryService.Queryable().ToList(), "Id", "Name");
            ViewBag.countryidc = new SelectList(_countryService.Queryable().ToList(), "Id", "Name");
            ViewBag.stateidl = new SelectList(_stateService.Queryable().ToList(), "Id", "Name", "id");
            ViewBag.stateidc = new SelectList(_stateService.Queryable().ToList(), "Id", "Name", "id");
            ViewBag.AccountingStandardId = new SelectList(_accountingStandardService.Queryable().ToList(), "Id", "Name");
            //ViewBag.CashAP = new SelectList(_companyAccountService.Queryable().Where(x => x.CompanyId).ToList(), "Id", "Name");
            IList<SelectListItem> list = Enum.GetValues(typeof(Common.PaymentFrequency)).Cast<Common.PaymentFrequency>().Select(x => new SelectListItem { Text = x.ToString(), Value = ((int)x).ToString() }).ToList();
            ViewBag.PaymentFrequencyNonLease = list;

        }
    }
}