﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Repository.Pattern.UnitOfWork;

namespace LeaseCrunch.Web.Controllers
{
    [Authorize]
    public class AccountingStandardsController : Controller
    {
        private LeaseCrunchContext db = new LeaseCrunchContext();

        // GET: AccountingStandards
        public async Task<ActionResult> Index()
        {
            return View(await db.AccountingStandards.ToListAsync());
        }

        // GET: AccountingStandards/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountingStandard accountingStandard = await db.AccountingStandards.FindAsync(id);
            if (accountingStandard == null)
            {
                return HttpNotFound();
            }
            return View(accountingStandard);
        }

        // GET: AccountingStandards/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AccountingStandards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Status")] AccountingStandard accountingStandard)
        {
            if (ModelState.IsValid)
            {
                db.AccountingStandards.Add(accountingStandard);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(accountingStandard);
        }

        // GET: AccountingStandards/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountingStandard accountingStandard = await db.AccountingStandards.FindAsync(id);
            if (accountingStandard == null)
            {
                return HttpNotFound();
            }
            return View(accountingStandard);
        }

        // POST: AccountingStandards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Status")] AccountingStandard accountingStandard)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountingStandard).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(accountingStandard);
        }

        // GET: AccountingStandards/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountingStandard accountingStandard = await db.AccountingStandards.FindAsync(id);
            if (accountingStandard == null)
            {
                return HttpNotFound();
            }
            return View(accountingStandard);
        }

        // POST: AccountingStandards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            AccountingStandard accountingStandard = await db.AccountingStandards.FindAsync(id);
            db.AccountingStandards.Remove(accountingStandard);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
