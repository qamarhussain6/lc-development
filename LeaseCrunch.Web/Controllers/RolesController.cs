﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using Repository.Pattern.UnitOfWork;
using LeaseCrunch.Web.Models;

namespace LeaseCrunch.Web.Controllers
{


    [Authorize(Roles = "Admin,A")]
    public class RolesController : Controller
    {
        //  private LeaseCrunchContext db = new LeaseCrunchContext();

        private readonly IModuleService _moduleService;
        private readonly IRolesRouteControlService _rolesRouteControlService;
        private readonly IRouteControlService _routeControlService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public RolesController(IUnitOfWorkAsync unitOfWorkAsync, IRolesRouteControlService rolesRouteControlService, IRouteControlService routeControlService, IModuleService moduleService)
        {
            _moduleService = moduleService;
            _unitOfWorkAsync = unitOfWorkAsync;
            _rolesRouteControlService = rolesRouteControlService;
            _routeControlService = routeControlService;
        }

        // GET: Roles
        public ActionResult Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            var roleStore = new RoleStore<IdentityRole>(db);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);
            var roles = roleMngr.Roles.ToList();
            return View(roles.ToList());

            //var adb = new Models.ApplicationDbContext();
            //var roles = adb.Roles;

            //return View(roles.ToList());
        }

        // GET: /Organization/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Organization/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Microsoft.AspNet.Identity.EntityFramework.IdentityRole role)
        {
            if (ModelState.IsValid)
            {
                //db.Organizations.Add(organization);
                //await db.SaveChangesAsync();
                var idManager = new IdentityManager();
                idManager.CreateRole(role.Name);
                return RedirectToAction("Index");
            }

            return View(role);
        }

        
        
        public ActionResult ManageRole(string id)
        {
            LeaseCrunchContext db = new LeaseCrunchContext();
            //var adb = new applicationdbcontext();
            var modules = db.Modules;
            //viewbag.rolesroutecontrols  =  db.rolesroutecontrols.where(r=>r.roleid == id).tolist();
            List<int> selectedroles = new List<int>();
            foreach (var item in db.RolesRouteControls.Where(r => r.RoleId == id).ToList())
                selectedroles.Add(item.RouteControlId);

            ViewBag.roleid = id;
            ViewBag.selectedroles = selectedroles;
            ViewBag.routecontrols = db.RouteControls.ToList();
            ViewBag.RoleId = id;
            return View(modules.ToList());
        }


        
        [HttpPost]
        public ActionResult ManageRole()
        {
            //var adb = new applicationdbcontext();
            ApplicationDbContext adb = new ApplicationDbContext();
            var modules = _moduleService.Queryable().ToList();
            var roleid = Request.Form["roleid"];

            foreach (var item in _rolesRouteControlService.Queryable().Where(r => r.RoleId == roleid).ToList())
            {
                _rolesRouteControlService.Delete(item);
            }
            _unitOfWorkAsync.SaveChanges();

            foreach (var routecontrol in _routeControlService.Queryable().ToList())
            {
                if (Request.Form[routecontrol.Id.ToString()] != null)
                {
                    var rolesroutecontrol = new RolesRouteControl()
                    {
                        RouteControlId = routecontrol.Id,
                        RoleId = roleid
                    };
                    _rolesRouteControlService.Insert(rolesroutecontrol);
                }
            }
            _unitOfWorkAsync.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}