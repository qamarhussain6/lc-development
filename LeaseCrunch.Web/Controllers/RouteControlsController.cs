﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeaseCrunch.Entities;
using LeaseCrunch.Service;
using Repository.Pattern.UnitOfWork;
using System.Web.Services;
using System.Web.Script.Services;
using System.Text;
using Repository.Pattern.Infrastructure;
using LeaseCrunch.Web.Models;
using System.Data.Entity.Validation;
using System.Net.Mail;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Reflection;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LeaseCrunch.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RouteControlsController : Controller
    {
        private LeaseCrunchContext db = new LeaseCrunchContext();
        private readonly IRouteControlService _routeControlService;
        private readonly IUnitOfWorkAsync _unitOfWorkAsync;

        public RouteControlsController(IUnitOfWorkAsync unitOfWorkAsync, IRouteControlService routeControlService)
        {
            _unitOfWorkAsync = unitOfWorkAsync;
            _routeControlService = routeControlService;
        }

        // GET: RouteControls
        public async Task<ActionResult> Index()
        {
            var routeControls = db.RouteControls.Include(r => r.Module);
            return View(await routeControls.ToListAsync());
        }

        // GET: RouteControls/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteControl routeControl = await db.RouteControls.FindAsync(id);
            if (routeControl == null)
            {
                return HttpNotFound();
            }
            return View(routeControl);
        }

        // GET: RouteControls/Create
        public ActionResult Create()
        {
            ViewBag.ModuleId = new SelectList(db.Modules, "Id", "Name");
            return View();
        }

        // POST: RouteControls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,ModuleId,Controller,Action,Description,status")] RouteControl routeControl)
        {

           _routeControlService.Insert(routeControl);

            try
            {
                await _unitOfWorkAsync.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                string error = string.Empty;
                foreach (var eve in e.EntityValidationErrors)
                {
                    error = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        error += string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {

                int i = 0;
                string s = ex.InnerException.ToString();
                s = ex.InnerException.Message;
                s = ex.InnerException.Source;
            }
/**************************************************/

            db.RouteControls.Add(routeControl);
            try
            {
                await db.SaveChangesAsync();
            }
            catch (Exception ex)
            { }
            return RedirectToAction("Index");

        }

        // GET: RouteControls/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            RouteControl routeControl = new RouteControl();
            routeControl = _routeControlService.Find(id);
            string status = Request.Form["status"]; 
            _routeControlService.Update(routeControl);
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //RouteControl routeControl = await db.RouteControls.FindAsync(id);
            if (routeControl == null)
            {
                return HttpNotFound();
            }
            ViewBag.ModuleId = new SelectList(db.Modules, "Id", "Name", routeControl.ModuleId);
            return View(routeControl);
        }

        // POST: RouteControls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,ModuleId,Controller,Action,Description,status")] RouteControl routeControl)
        {
            if (ModelState.IsValid)
            {
                db.Entry(routeControl).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.ModuleId = new SelectList(db.Modules, "Id", "Name", routeControl.ModuleId);
            return View(routeControl);
        }

        // GET: RouteControls/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RouteControl routeControl = await db.RouteControls.FindAsync(id);
            if (routeControl == null)
            {
                return HttpNotFound();
            }
            return View(routeControl);
        }

        // POST: RouteControls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            RouteControl routeControl = await db.RouteControls.FindAsync(id);
            db.RouteControls.Remove(routeControl);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
