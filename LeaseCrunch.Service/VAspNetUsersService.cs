﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class VAspNetUsersService : Service<VAspNetUser>, IVAspNetUsersService
    {
        private readonly IRepositoryAsync<VAspNetUser> _repository;

        public VAspNetUsersService(IRepositoryAsync<VAspNetUser> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
