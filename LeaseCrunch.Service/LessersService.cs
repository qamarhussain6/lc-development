﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LessersService : Service<Lesser>, ILessersService
    {
        private readonly IRepositoryAsync<Lesser> _repository;

        public LessersService(IRepositoryAsync<Lesser> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
