﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class AspNetUserService : Service<AspNetUser>, IAspNetUserService
    {
        private readonly IRepositoryAsync<AspNetUser> _repository;

        public AspNetUserService(IRepositoryAsync<AspNetUser> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
