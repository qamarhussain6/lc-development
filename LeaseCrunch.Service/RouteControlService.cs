﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class RouteControlService : Service<RouteControl>, IRouteControlService
    {
        private readonly IRepositoryAsync<RouteControl> _repository;

        public RouteControlService(IRepositoryAsync<RouteControl> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}