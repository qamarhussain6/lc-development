﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CustomizationCustomFieldService : Service<CustomizationCustomField>, ICustomizationCustomFieldService
    {
        private readonly IRepositoryAsync<CustomizationCustomField> _repository;

        public CustomizationCustomFieldService(IRepositoryAsync<CustomizationCustomField> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
