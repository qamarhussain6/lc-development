﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class ResellerService : Service<Reseller>, IResellerService
    {
        private readonly IRepositoryAsync<Reseller> _repository;

        public ResellerService(IRepositoryAsync<Reseller> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}