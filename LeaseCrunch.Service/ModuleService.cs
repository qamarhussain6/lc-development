﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class ModuleService : Service<Module>, IModuleService
    {
        private readonly IRepositoryAsync<Module> _repository;

        public ModuleService(IRepositoryAsync<Module> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
