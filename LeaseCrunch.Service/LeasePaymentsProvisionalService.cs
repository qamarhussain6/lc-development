﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LeasePaymentsProvisionalService : Service<LeasePaymentsProvisional>, ILeasePaymentsProvisionalService
    {
        private readonly IRepositoryAsync<LeasePaymentsProvisional> _repository;

        public LeasePaymentsProvisionalService(IRepositoryAsync<LeasePaymentsProvisional> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}