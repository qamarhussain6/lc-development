﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class VResellerService : Service<VReseller>, IVResellerService
    {
        private readonly IRepositoryAsync<VReseller> _repository;

        public VResellerService(IRepositoryAsync<VReseller> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
