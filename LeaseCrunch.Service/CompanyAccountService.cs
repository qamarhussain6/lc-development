﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CompanyAccountService : Service<CompanyAccount>, ICompanyAccountService
    {
        private readonly IRepositoryAsync<CompanyAccount> _repository;

        public CompanyAccountService(IRepositoryAsync<CompanyAccount> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
