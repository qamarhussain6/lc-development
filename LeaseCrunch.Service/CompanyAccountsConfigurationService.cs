﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CompanyAccountsConfigurationService : Service<CompanyAccountsConfiguration>, ICompanyAccountsConfigurationService
    {
        private readonly IRepositoryAsync<CompanyAccountsConfiguration> _repository;

        public CompanyAccountsConfigurationService(IRepositoryAsync<CompanyAccountsConfiguration> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
