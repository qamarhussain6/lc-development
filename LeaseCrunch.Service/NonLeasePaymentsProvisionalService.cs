﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class NonLeasePaymentsProvisionalService : Service<NonLeasePaymentsProvisional>, INonLeasePaymentsProvisionalService
    {
        private readonly IRepositoryAsync<NonLeasePaymentsProvisional> _repository;

        public NonLeasePaymentsProvisionalService(IRepositoryAsync<NonLeasePaymentsProvisional> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}