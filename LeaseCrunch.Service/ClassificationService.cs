﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class ClassificationService : Service<Classification>, IClassificationService
    {
        private readonly IRepositoryAsync<Classification> _repository;

        public ClassificationService(IRepositoryAsync<Classification> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
