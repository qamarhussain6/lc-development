﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CustomizationCostCenterService : Service<CustomizationCostCenter>, ICustomizationCostCenterService
    {
        private readonly IRepositoryAsync<CustomizationCostCenter> _repository;

        public CustomizationCostCenterService(IRepositoryAsync<CustomizationCostCenter> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
