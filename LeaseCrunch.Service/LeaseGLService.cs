﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LeaseGLService : Service<LeaseGLAccount>, ILeaseGLService
    {
        private readonly IRepositoryAsync<LeaseGLAccount> _repository;

        public LeaseGLService(IRepositoryAsync<LeaseGLAccount> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
