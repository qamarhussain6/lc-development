﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class VCustomerService : Service<VCustomer>, IVCustomerService
    {
        private readonly IRepositoryAsync<VCustomer> _repository;

        public VCustomerService(IRepositoryAsync<VCustomer> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}