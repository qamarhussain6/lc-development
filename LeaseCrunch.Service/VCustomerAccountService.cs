﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class VCustomerAccountService : Service<VCustomerAccount>, IVCustomerAccountService
    {
        private readonly IRepositoryAsync<VCustomerAccount> _repository;

        public VCustomerAccountService(IRepositoryAsync<VCustomerAccount> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
