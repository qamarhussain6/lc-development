﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class AspNetRoleService : Service<AspNetRole>, IAspNetRoleService
    {
        private readonly IRepositoryAsync<AspNetRole> _repository;

        public AspNetRoleService(IRepositoryAsync<AspNetRole> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
