﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class AccountingStandardService : Service<AccountingStandard>, IAccountingStandardService
    {
        private readonly IRepositoryAsync<AccountingStandard> _repository;

        public AccountingStandardService(IRepositoryAsync<AccountingStandard> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
