﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LocationService : Service<Location>, ILocationService
    {
        private readonly IRepositoryAsync<Location> _repository;

        public LocationService(IRepositoryAsync<Location> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
