﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class AccountTypeService : Service<AccountType>, IAccountTypeService
    {
        private readonly IRepositoryAsync<AccountType> _repository;

        public AccountTypeService(IRepositoryAsync<AccountType> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
