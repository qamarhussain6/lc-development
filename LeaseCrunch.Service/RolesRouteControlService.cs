﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class RolesRouteControlService : Service<RolesRouteControl>, IRolesRouteControlService
    {
        private readonly IRepositoryAsync<RolesRouteControl> _repository;

        public RolesRouteControlService(IRepositoryAsync<RolesRouteControl> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}