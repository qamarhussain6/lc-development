﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LeaseDocumentsService : Service<LeaseDocument>, ILeaseDocumentsService
    {
        private readonly IRepositoryAsync<LeaseDocument> _repository;

        public LeaseDocumentsService(IRepositoryAsync<LeaseDocument> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
