﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LeasesService : Service<Lease>, ILeasesService
    {
        private readonly IRepositoryAsync<Lease> _repository;

        public LeasesService(IRepositoryAsync<Lease> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
