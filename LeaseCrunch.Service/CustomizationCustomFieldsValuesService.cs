﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CustomizationCustomFieldsValuesService : Service<CustomizationCustomFieldsValue>, ICustomizationCustomFieldsValuesService
    {
        private readonly IRepositoryAsync<CustomizationCustomFieldsValue> _repository;

        public CustomizationCustomFieldsValuesService(IRepositoryAsync<CustomizationCustomFieldsValue> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
