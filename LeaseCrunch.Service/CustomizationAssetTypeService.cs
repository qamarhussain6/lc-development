﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CustomizationAssetTypeService : Service<CustomizationAssetType>, ICustomizationAssetTypeService
    {
        private readonly IRepositoryAsync<CustomizationAssetType> _repository;

        public CustomizationAssetTypeService(IRepositoryAsync<CustomizationAssetType> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
