﻿using LeaseCrunch.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public interface IAspNetUserRoleService : IService<AspNetUserRole>
    {

    }
}
