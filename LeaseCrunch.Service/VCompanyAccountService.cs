﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class VCompanyAccountService : Service<VCompanyAccount>, IVCompanyAccountService
    {
        private readonly IRepositoryAsync<VCompanyAccount> _repository;

        public VCompanyAccountService(IRepositoryAsync<VCompanyAccount> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
