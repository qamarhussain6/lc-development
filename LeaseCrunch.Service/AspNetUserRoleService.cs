﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class AspNetUserRoleService : Service<AspNetUserRole>, IAspNetUserRoleService
    {
        private readonly IRepositoryAsync<AspNetUserRole> _repository;

        public AspNetUserRoleService(IRepositoryAsync<AspNetUserRole> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
