﻿using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class CustomizationLessorService : Service<Lesser>, ICustomizationLessorService
    {
        private readonly IRepositoryAsync<Lesser> _repository;

        public CustomizationLessorService(IRepositoryAsync<Lesser> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
