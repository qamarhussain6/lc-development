﻿using System;
using LeaseCrunch.Entities;
using Repository.Pattern.Repositories;
using Service.Pattern;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaseCrunch.Service
{
    public class LeaseDocumentService : Service<LeaseDocument>, ILeaseDocumentService
    {
        private readonly IRepositoryAsync<LeaseDocument> _repository;

        public LeaseDocumentService(IRepositoryAsync<LeaseDocument> repository) : base(repository)
        {
            _repository = repository;
        }
    }
}
