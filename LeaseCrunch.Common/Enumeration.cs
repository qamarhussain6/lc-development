﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LeaseCrunch.Common
{
    public enum UserLevel
    {
        LCAdmin = 0,
        Reseller = 1,
        Customer = 2,
        Customer_User = 3,
        Customer_Readonly = 4
    }

    public enum UserStatus
    {
        Active = 1,
        Disabled = 0
    }

    public enum Leases
    {
        Leases_Operating_Leases = 4,
        Leases_Finance_Leases = 5,
        Non_Leases_Operating_Leases = 6,
        Non_Leases_Finance_Leases = 7,
        Classification =25,
        FinancialStatement =26
    }

    public enum PaymentFrequency
    {
        Monthly = 1,
        Quarterly = 2,
        Yearly = 3
    }

}